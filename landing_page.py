import time
import os
import lxml.html
from funcs import parse_content

# pylint: disable=unused-argument
class LandingPageBase():
    def __init__(self, content, parent):
        self.init()

        self.log = parent.log
        self.name = parent.name
        self.landing_page = parent.landing_page
        self.base_url = parent.base_url
        self.xml = parent.xml
        self.json_redirect = parent.json_redirect
        self.main = parent.main
        self.user_agents = parent.user_agents
        self.article_parser = parent.article_parser
        self.parent = parent

        self.content_raw = content
        self.content = parse_content(content, parent.xml)
        self.crawled = 0
        self.saved = 0
        self.found = 0
        self.pos = 0

        self.url = None

        self.crawl()



    def init(self):
        pass



    def crawl(self):
        found_urls = []

        count = 0

        # get list of articles from landing page
        article_list = self.get_article_list()

        if not article_list or len(article_list) < 1:
            self.log.error("%s article list parsing error!", self.name)
            raise ValueError(self.name + " article list parsing error!")


        # iterate over articles and crawl their details
        for article in article_list:
            # write dump file for debug if no top article found
            if count == 10 and self.pos == 0:
                path = os.path.join(self.main.path, "dumps", "no_top_article_dump_"+str(time.time())+'.html')

                with open(path, "wb") as dump:
                    dump.write(lxml.html.tostring(self.content, encoding="utf-8"))

                self.log.error("%s can't find top article!", self.name)
                return

            self.url = self.get_url(article)

            # add base url if link is relative
            if self.url.startswith("/") and not self.url.startswith("//"):
                self.url = self.base_url + self.url

            # ignore third party sites
            elif not self.url.startswith(self.base_url) and not hasattr(self.parent, 'content_url'):
                #self.log.warning("Link to 3rd party site - ignoring %s", self.url)
                continue

            # verify for positioning and other attributes
            if not self.verify(article):
                if self.pos == 0:
                    count += 1
                continue

            self.pos += 1

            # skip article parsing if already known
            if self.url in found_urls:
                continue

            found_urls.append(self.url)


            # if --landing argument is used, only show position and url and continue
            if self.main.args.landing:
                print(self.pos, self.url)
                continue

            # save the position of the url on the landing page
            self.save_position(self.pos, self.url)


            if self.url in self.parent.known_urls:
                continue

            self.found += 1

            # put the site and the callback in request queue
            self.main.queue_request.put([
                self.article_parser,
                [self, self.url],
                self.url,
                self.user_agents,
                self.json_redirect
            ])

        # save crawl_result even if nothing found
        if self.found == 0:
            self.save_crawl_result()

        # parse urls that were known, but removed from the landing page
        # so changes can be tracked while saving traffic because the page
        # isn't crawled every iteration
        for url in self.parent.known_urls:
            # only parse articles not found in current landing page
            if url in found_urls:
                continue

            # put the site and the callback in request queue
            self.main.queue_request.put([
                self.article_parser,
                [self, url, False],
                url,
                self.user_agents,
                self.json_redirect
            ])

            # remove url from known urls list
            self.parent.known_urls.remove(url)

        # add found urls to known urls
        self.parent.known_urls = list(set(self.parent.known_urls + found_urls))




    def save_position(self, pos, url):
        data = {
            "url"       : url,
            "position"  : pos
        }

        self.main.queue_save.put(['position', data], True)



    def save_crawl_result(self):
        data = {
            "source"            : self.name,
            "articles_found"    : self.found,
            "articles_saved"    : self.saved
        }

        if self.main.args.site:
            print(f"\n{self.name} finished FOUND {self.found} SAVED {self.saved}")

        self.main.queue_save.put(['crawl_result', data], True)



    def get_article_list(self):
        return []



    def get_url(self, article):
        return article.attrib['href']



    def verify(self, article):
        return True
