diff_match_patch>=20230430
lxml>=4.9.2
numpy>=1.26.1
PyMySQL>=1.1.0
python_daemon>=3.0.1
scikit_learn>=1.3.1
spacy>=3.7.1
