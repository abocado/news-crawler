#!/usr/bin/env python3
# _*_ coding:utf-8 _*_

import pymysql


#pylint: disable=invalid-name, bare-except, broad-exception-caught
class Mysql():
    def __init__(self):
        self.mysql_host = \
        self.mysql_user = \
        self.mysql_pw = \
        self.mysql_db = \
        self.timeout = \
        self.cur = \
        self.db = None

        self.autocommit = True



    #pylint: disable=too-many-arguments
    def connect(self, mysql_host, mysql_user, mysql_pw, mysql_db = None, timeout = None):
        self.mysql_host = mysql_host
        self.mysql_user = mysql_user
        self.mysql_pw = mysql_pw
        self.mysql_db = mysql_db
        self.timeout = timeout

        self.cur = False
        
        if not self._connect():
            return False

        return True



    def _connect(self):
        try:
            self.db = pymysql.connect(
                host=self.mysql_host,
                user=self.mysql_user,
                passwd=self.mysql_pw,
                db=self.mysql_db,
                cursorclass=pymysql.cursors.DictCursor,
                connect_timeout=self.timeout,
                write_timeout=self.timeout,
                read_timeout=self.timeout,
                charset='utf8mb4',
                autocommit=self.autocommit
            )

            self.cur = self.db.cursor()

            try:
                self.db.set_charset('utf8mb4')
            except:
                pass

            self.cur.execute('SET NAMES utf8mb4;')
            self.cur.execute('SET CHARACTER SET utf8mb4;')
            self.cur.execute('SET character_set_connection=utf8mb4;')
            return True
        except Exception as mysql_error:
            print("Connecting to MySQL failed.", mysql_error)
            return False



    def exec(self, query, data = ()):
        try:
            self.db.ping(True)

            if not self.db.open:
                print("Connection closed, trying to reconnect")
                if not self._connect():
                    return False

            return self.cur.execute(query, data)

        except Exception as mysql_error:
            print("MySQL Exception:", mysql_error, "Conn Status:", self.db.open, "Query:", query)
            return False



    def save(self, query, data = ()):
        result = self.exec(query, data)

        #if self.autocommit:
        #    self.db.commit()

        if result is False:
            return False

        return self.cur.lastrowid




    def get(self, query, data = ()):
        result = self.exec(query, data)

        if result is False:
            return False

        return self.cur.fetchall()



    def call(self, proc, args = ()):
        result = self.cur.callproc(proc, args)

        if result is False:
            return False

        return self.cur.fetchall()



    def commit(self):
        return self.db.commit()




    def escape(self, data):
        try:
            return self.db.escape_string(data).decode('utf-8')
        except:
            return self.db.escape_string(data)




    def close(self):
        self.cur.close()
        self.db.close()
