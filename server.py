import json
from http.server import HTTPServer, BaseHTTPRequestHandler



class Server():
    def __init__(self, main):
        def handler(*args):
            JSONRequestHandler(main, *args)

        # Create an HTTP server with the JSONRequestHandler as the handler
        httpd = HTTPServer(('localhost', main.args.port), handler)

        # Start the HTTP server
        httpd.serve_forever()




class JSONRequestHandler(BaseHTTPRequestHandler):
    def __init__(self, main, *args):
        self.main = main
        BaseHTTPRequestHandler.__init__(self, *args)


    #pylint: disable=invalid-name
    def do_GET(self):
        # Set the response status code
        self.send_response(200)

        # Set the response header as JSON
        self.send_header('Content-type', 'application/json')
        self.end_headers()

        # Create a JSON object to send as the response
        response_data = self.main.print_json_status()

        # Write the JSON object to the response body
        self.wfile.write(json.dumps(response_data).encode())


    # mute
    def log_message(self, *args):
        return
