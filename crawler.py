import time
import datetime

class CrawlerBase():
    def __init__(self, main, landing_page_parser, article_parser):
        self.user_agents = None

        self.main = main

        self.init()

        self.article_parser = article_parser
        self.landing_page_parser = landing_page_parser

        if not hasattr(self, 'name'):
            self.name = None

        if not hasattr(self, 'landing_page'):
            self.landing_page = None

        if not hasattr(self, 'base_url'):
            self.base_url = None

        if not hasattr(self, 'xml'):
            self.xml = False

        if not hasattr(self, 'json_redirect'):
            self.json_redirect = False

        self.log = self.main.log

        if self.name in self.main.known_urls:
            self.known_urls = self.main.known_urls[self.name]
            # free memory
            del self.main.known_urls[self.name]
        else:
            self.known_urls = []

        # if url argument is given, only the url is parsed then exit
        if self.main.args.url and self.main.args.site:
            self.main.queue_request.put([
                self.article_parser,
                [self, self.main.args.url, False],
                self.main.args.url,
                None
            ])

        else:
            self.loop()



    def init(self):
        pass



    def loop(self):
        date = None

        while True:
            # wait until queue is not so full anymore
            while self.main.queue_save.qsize() > self.main.save_queue_limit \
            or self.main.queue_request.qsize() > self.main.request_queue_limit:
                time.sleep(10)

            # put the landing page and the callback in request queue
            self.main.queue_request.put([
                self.landing_page_parser,
                [self],
                self.landing_page,
                self.user_agents
            ])

            # save abbreviations once a day
            today = datetime.date.today()
            if date != today:
                date = today
                self.get_author_detail()

            # sleep for crawl_interval (minutes)
            time.sleep(self.main.crawl_interval * 60)



    def get_author_detail(self):
        return None
