#!/usr/bin/env python3
# _*_ coding:utf-8 _*_
#pylint: disable=import-outside-toplevel, unused-variable, pointless-string-statement
#pylint: disable=unreachable, invalid-name, broad-exception-caught, bare-except

import re
import sys
from time import sleep

import spacy
import numpy as np
from sklearn.cluster import HDBSCAN#, DBSCAN, KMeans
from sklearn.feature_extraction.text import TfidfVectorizer#, HashingVectorizer, CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity#, pairwise_distances_argmin_min

from mysql_lib import Mysql


class Grouping:
    def __init__(self, parent):
        self.main = parent

        self.debug = False

        if __name__ == '__main__':
            self.debug = True
            self.debug_titles_reduced = {}
            self.debug_groups = {}
            self.debug_group_labels = {}

        # use word lemmas (root / base of word)
        self.lemmatize = True
        # minimal amount of words in a filtered title
        self.min_filtered_title_word_count = 3
        # minimal length of any title word
        self.min_word_length = 2

        self.days = 0

        self.settings = {
            1   : {
                'interval'                  : 1,
                'max_articles'              : 300,
                'max_articles_per_source'   : 3,
                'min_similarity'            : 1.2,
                'min_articles_per_group'    : 3,
                'max_articles_per_group'    : 15,
                'filter_nouns_only'         : False
            },

            7   : {
                'interval'                  : 12,
                'max_articles'              : 500,
                'max_articles_per_source'   : 3,
                'min_similarity'            : 1.3,
                'min_articles_per_group'    : 3,
                'max_articles_per_group'    : 7,
                'filter_nouns_only'         : True
            },

            30   : {
                'interval'                  : 24,
                'max_articles'              : 1000,
                'max_articles_per_source'   : 3,
                'min_similarity'            : 1.3,
                'min_articles_per_group'    : 3,
                'max_articles_per_group'    : 5,
                'filter_nouns_only'         : True
            }
        }


        self.titles = []
        self.title_data = []
        self.groups = {}
        self.group_weight = {}

        self.stopwords = []
        self.compound_split = {}

        self.nlp = spacy.load('de_core_news_sm')

        self.load_lists()

        self.filter_words = [
            '+++',
            's+',
            'stern+',
            '+',
            'faz',
            'bild',
            'taz',
            'sz',
            'live',
            'ticker',
            'liveticker',
            'news',
            'nachrichten',
            'liveblog'
        ]

        self.blacklist_category = [
            'Panorama',
            'Lifestyle',
            'Sport',
            'Unterhaltung',
            'Promis',
            'Shopping & Service'
        ]

        self.db = Mysql()

        if not self.db.connect(
            self.main.mysql_host,
            self.main.mysql_user,
            self.main.mysql_pw,
            self.main.mysql_db,
            self.main.mysql_timeout
        ):
            self.main.log.error("No database connection. Shutting down...")
            sys.exit(1)

        self.loop()



    def loop(self):
        interval = 0

        while True:
            for days, settings in self.settings.items():
                if interval % settings['interval'] != 0:
                    continue

                self.days = days

                # retrieve data from database or file
                self.get_data()

                if len(self.titles) < 50:
                    continue

                # do ML magic
                if not self.process():
                    continue
                # sort
                self.sort_groups()
                # save to db
                self.save()

                sleep(10) # let the db breathe ;-)

            if self.debug:
                return

            sleep(self.main.crawl_interval * 60)



    def load_lists(self):
        if self.main.stop_word_list:
            self.stopwords = open(self.main.stop_word_list, encoding="utf-8").readlines()

        if not self.main.compound_split_list:
            return

        with open(self.main.compound_split_list, encoding="utf-8") as compound_split_list:
            for line in compound_split_list:
                compound_elements = line.rstrip().lower().split("\t")
                self.compound_split[compound_elements.pop(0)] = compound_elements




    def filter(self, title):
        title = title.replace("-", " ") \
                     .replace("+", " ") \
                     .replace(".", "") \
                     .replace(":", " ") \
                     .replace("\"", "") \
                     .replace("'", "")

        processed_title = self.nlp(title)
        filtered_title = []

        for word in processed_title:
            if self.lemmatize:
                word_lower = word.lemma_.lower()
            else:
                word_lower = word.text.lower()

            if (
                word.is_stop
                or word.is_punct
                or word.is_space
                or re.match(r'^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$', word_lower) #time
                or word_lower in self.stopwords
                or word_lower in self.filter_words
                or len(word_lower) < self.min_word_length
            ):
                continue

            if self.settings[self.days]['filter_nouns_only'] \
            and (
                word.pos_ != "NOUN"
                and
                word.pos_ != "PROPN"
            ):
                continue

            if word_lower in self.compound_split:
                for component in self.compound_split[word_lower]:
                    filtered_title.append(component.split('|')[0])
            else:
                filtered_title.append(word_lower)

        # we only need unique words in title
        filtered_title = list(set(filtered_title))

        if len(filtered_title) < self.min_filtered_title_word_count:
            return ""

        return " ".join(filtered_title)



    def get_data(self):
        self.titles = []
        self.title_data = []
        self.groups = {}
        self.group_weight = {}


        qry = "DROP TEMPORARY TABLE IF EXISTS article_data;"
        self.db.get(qry)

        if self.days == 1:
            visible = '''
                JOIN
                    visible
                    ON articles.id = visible.article_id
            '''

            position_weight = '''
                (
                    visible.weight 
                    *
                    (sources.views * 100 / (SELECT SUM(views) FROM sources))
                )
            '''

        else:
            visible = ""
            position_weight = f'''
                (
                    (
                        articles.weight
                        /
                        articles.hours
                    )
                    *
                    (
                        sources.views * 100 
                        / 
                        (SELECT SUM(views) FROM sources)
                    )
                )
            '''


        qry = f'''
            CREATE TEMPORARY TABLE
                article_data (UNIQUE id (article_id))
            SELECT
                articles.id AS article_id,
                articles.source,
                articles.title,
                {position_weight} AS position_weight
            FROM
                articles
            JOIN
                sources
                ON articles.source = sources.id
            {visible}
            WHERE
                articles.date_published >= (NOW() - INTERVAL {self.days} DAY)
            GROUP BY
                articles.id;
        '''

        self.db.get(qry)


        qry = f'''
            SELECT
                article_data.article_id,
                article_data.title,
                article_data.source,
                article_data.position_weight,
                categories.name as category_name
            FROM
                article_data
            JOIN
                category_links
                ON article_data.article_id = category_links.article_id
            JOIN
                categories
                ON category_links.category_id = categories.id
            WHERE
                article_data.position_weight IS NOT NULL
            GROUP BY
                article_data.article_id
            ORDER BY
                article_data.position_weight DESC
            LIMIT {self.settings[self.days]['max_articles']};
        '''

        data = self.db.get(qry)


        if not data:
            print("MYSQL ERROR")
            return False

        for row in data:
            if row['category_name'] in self.blacklist_category:
                continue

            article_id = row['article_id']
            title = self.filter(row['title'])
            weight = round(row['position_weight'])
            source = row['source']

            if not title:
                continue

            self.titles.append(title)

            self.title_data.append({
                'article_id': article_id,
                'weight': weight,
                'source': source,
                'original_title': row['title']
            })

        return True


    '''
    def plot(self, X_arr, medoids, labels):
        from sklearn.decomposition import PCA
        import matplotlib.pyplot as plt

        pca = PCA(n_components=2).fit(X_arr)
        data2D = pca.transform(X_arr)
        centers2D = pca.transform(medoids)

        plt.figure(figsize=(16, 12))
        plt.scatter(data2D[:,0], data2D[:,1], c=labels.astype(float))
        plt.scatter(centers2D[:,0], centers2D[:,1], 
                    marker='x', s=200, linewidths=3, c='r')

        for i, txt in enumerate(self.titles):
            #if clusterer.labels_[i] < 0 or clusterer.probabilities_[i] < 0.5:
            #    continue

            plt.annotate(txt, (data2D[i,0], data2D[i,1]))

        plt.show()
    '''



    def process(self):
        vectorizer = TfidfVectorizer()
            #min_df=5
            #max_df=0.3
            #ngram_range=(2, 2),
            #use_idf=True,
        #)

        X = vectorizer.fit_transform(self.titles)
        X_arr = X.toarray()
        
        if 0 in X_arr.shape:
            self.main.log.error("0 in shape of TfidfVectorizer")
            return False

        # cluster
        clusterer = HDBSCAN(
            metric='euclidean',
            cluster_selection_method="leaf",
            store_centers='medoid',
            min_samples=3
        ).fit(X_arr)

        # calculate distance to medoids. this is added to the score from clusterer.probabilities_
        # to rate the headline
        similarity = cosine_similarity(X_arr, clusterer.medoids_)

        #indices_to_keep = []

        filtered_titles = []
        filtered_title_data = []
        count = 0

        # save groups / clusters to dict
        for i, _ in enumerate(self.titles):
            cl_nr = int(clusterer.labels_[i])
            if cl_nr < 0:
                continue

            score = clusterer.probabilities_[i] + similarity[i][cl_nr]

            # threshold
            if score < self.settings[self.days]['min_similarity']:
                continue

            # add title id and score to group
            #indices_to_keep.append(i)
            filtered_titles.append(self.titles[i])
            filtered_title_data.append(self.title_data[i])

            self.groups.setdefault(cl_nr, {})[count] = score

            # add article weight to group
            self.group_weight[cl_nr] = self.group_weight.get(cl_nr, 0) \
                                     + self.title_data[i]['weight']

            count += 1

        self.titles = filtered_titles
        self.title_data = filtered_title_data
        
        if self.debug:
            print(self.days, "SHAPE", X_arr.shape, len(self.titles))

        #X_arr = X_arr[indices_to_keep]
        #medoids = clusterer.medoids_
        #labels = clusterer.labels_[indices_to_keep]

        #self.plot(X_arr, medoids, labels) #DEBUG

        #sys.exit()

        return True




    def sort_groups(self):
        new_groups = []

        for group in sorted(self.group_weight, key=self.group_weight.get, reverse=True):
            source_count = {}
            cur_group = []
            cluster_count = 0

            # sort by best to lowest score
            sorted_groups = sorted(
                self.groups[group].items(),
                key=lambda item: item[1],
                reverse=True
            )

            for title_id, score in sorted_groups:
                source = self.title_data[title_id]['source']
                source_count[source] = source_count.get(source, 0) + 1

                # dont adopt title if there are already at least max_articles_per_source
                # articles from that source in this group
                if source_count[source] > self.settings[self.days]['max_articles_per_source']:
                    continue

                cluster_count += 1
                if cluster_count > self.settings[self.days]['max_articles_per_group']:
                    break

                cur_group.append((title_id, score))

            if len(cur_group) > self.settings[self.days]['min_articles_per_group']:
                new_groups.append(cur_group)

        self.groups = new_groups




    def save(self):
        # save groups
        self.db.get('DELETE FROM groups WHERE days = %s;', (self.days,))

        qry = '''
            INSERT INTO
                groups
                (days, group_nr, rank, article_id)
            VALUES
                (%s, %s, %s, %s);
        '''

        for group_nr, group in enumerate(self.groups):
            if self.debug:
                print(f"\nGROUP {group_nr+1}")

            rank = 0

            for title_id, score in group:
                rank += 1

                if self.debug:
                    print(score, self.titles[title_id])

                article_id = self.title_data[title_id]['article_id']
                values = (self.days, group_nr + 1, rank, article_id)

                result = self.db.save(qry, values)

                if result is False:
                    return False

        # tell news-crawler a group was saved successfully
        self.main.saved_groups += 1

        return True









# debugging
class main:
    def __init__(self):
        self.path = os.path.dirname(os.path.abspath(__file__))
        self.config = get_config(self.path)
        self.mysql_timeout = self.config.getint('mysql', 'mysql_timeout')
        self.mysql_user = self.config.get('mysql', 'mysql_user')
        self.mysql_pw = self.config.get('mysql', 'mysql_pw')
        self.mysql_db = self.config.get('mysql', 'mysql_db')
        self.mysql_host = self.config.get('mysql', 'mysql_host')

        self.saved_groups = 0
        self.crawl_interval = 30

        self.stop_word_list = self.config.get('grouping', 'stop_word_list',
                                                        fallback="")
        self.compound_split_list = self.config.get('grouping', 'compound_split_list',
                                                        fallback="")

        grp = Grouping(self)





if __name__ == '__main__':
    import os
    from funcs import get_config

    main()
