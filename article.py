from pprint import pprint
from funcs import get_ld_json, parse_content

class ArticleBase:
    def __init__(self, content, parent, url, register = True):
        self.parent = parent
        self.main = parent.main
        self.url = url
        self.content_raw = content
        self.content = parse_content(content)
        self.register = register

        self.json = None
        self.json_extra = None

        self.crawl_article()

        if self.register:
            self.parent.crawled += 1

            if self.parent.crawled == self.parent.found:
                self.parent.save_crawl_result()



    def crawl_article(self):
        #pylint: disable=assignment-from-none
        if self.main.args.article or self.main.args.url:
            print("URL", self.url)

        # dummy function to parse json
        if not self.parse_json():
            return False

        # url
        #original_url = self.url
        #url = self.get_permalink()

        # title
        title = self.get_title()

        # description
        description = self.get_description()

        # image url
        image_url = self.get_image_url()

        # date modified
        date_modified = self.get_date_modified()

        # date published
        date_published = self.get_date_published()

        # categories
        categories = self.get_categories()

        # internal id
        internal_id = self.get_internal_id()

        # author
        authors = self.get_authors()

        # tags
        tags = self.get_tags()

        # content ( full text )
        content = self.get_content()

        # paywall
        paywall = self.is_paywall()

        # opinion
        opinion = self.is_opinion()


        # data checks
        if not image_url:
            image_url = None

        if not content:
            content = None

        '''
        if not url:
            self.parent.log.error("NO URL FOUND! %s", original_url)
            if not original_url:
                raise ValueError("NO URL FOUND!")

            self.url = url = original_url
        '''

        if not title:
            self.parent.log.error("NO TITLE FOUND! %s", self.url)
            raise ValueError("NO TITLE FOUND!")

        if not description:
            self.parent.log.error("NO DESCRIPTION FOUND! %s", self.url)
            return False

        if not internal_id:
            self.parent.log.error("NO INTERNAL_ID FOUND! %s", self.url)
            return False

        if not date_published:
            self.parent.log.error("NO DATE FOUND! %s", self.url)
            return False

        if not date_modified:
            date_modified = date_published


        data = {
            "source":           self.parent.name,
            "url":              self.url, #url
            "image_url":        image_url,
            "authors":          authors,
            "tags":             tags,
            "categories":       categories,
            "title":            title,
            "description":      description,
            "date_modified":    date_modified,
            "date_published":   date_published,
            "internal_id":      internal_id,
            "content":          content,
            "paywall":          paywall,
            "opinion":          opinion
        }

        self.main.queue_save.put(['article', data], True)

        if self.main.args.article or self.main.args.url:
            pprint(data)
            print()

        if self.register:
            self.parent.saved += 1

        return True




    def parse_json(self):
        return self._parse_json()



    def _parse_json(self, mandatory = False):
        if not mandatory:
            mandatory = {
                'NewsArticle': [
                    'dateModified'#,
                    #'author'
                ]
            }

        self.json = get_ld_json(self.content)

        if not self.json:
            self.parent.log.debug("No JSON Data found! Skipping: %s", self.url)
            return False

        # find alternative named NewsArticle key if any
        if "NewsArticle" in mandatory and not "NewsArticle" in self.json:
            for _type in self.json:
                if 'dateModified' in self.json[_type] or 'uploadDate' in self.json[_type]:
                    self.json["NewsArticle"] = self.json[_type]

                    if "uploadDate" in self.json["NewsArticle"] \
                    and not 'dateModified' in self.json["NewsArticle"]:
                        self.json["NewsArticle"]["datePublished"] = self.json["NewsArticle"]["uploadDate"]
                        self.json["NewsArticle"]["dateModified"] = self.json["NewsArticle"]["uploadDate"]

                    break

        # check if all mandatory lists are present
        for list_name in mandatory:
            if list_name not in self.json:
                self.parent.log.debug(f"{list_name} not found in JSON Data! Skipping: %s", self.url)
                return False

            for key in mandatory[list_name]:
                if key not in self.json[list_name]:
                    self.parent.log.debug(f"{key} not found in JSON Data! Skipping: %s", self.url)
                    return False

        return True



    def get_internal_id(self):
        return None



    def get_permalink(self):
        return self.url



    def get_title(self):
        title_el = self.content.xpath('.//meta[@property="og:title"]')
        if not title_el:
            return None

        return title_el[0].attrib["content"].strip()



    def get_description(self):
        desc_el = self.content.xpath('.//meta[@property="og:description" and @content]')
        if not desc_el:
            return ""

        return desc_el[0].attrib["content"].strip()



    def get_image_url(self):
        image_el = self.content.xpath('.//meta[@property="og:image"]')

        if not image_el:
            return None

        return image_el[0].attrib["content"].strip()



    def get_date_modified(self):
        return False



    def get_date_published(self):
        return False



    def get_categories(self):
        return False



    def get_authors(self):
        return False



    def get_tags(self):
        return False



    def get_content(self):
        return False



    def is_paywall(self):
        return None



    def is_opinion(self):
        opinion = self.content.xpath('.//meta[@property="article:opinion"]')
        if isinstance(opinion, list) and len(opinion) == 1:
            return opinion[0].attrib["content"].strip().lower() == 'true'

        return None
