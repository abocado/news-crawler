import time
import json
import datetime
import hashlib
from diff_match_patch import diff_match_patch


# pylint: disable=too-many-instance-attributes,invalid-name
class Save:
    def __init__(self, main):
        self.main = main

        # setup logger
        self.log = self.main.log

        # mysql db
        self.db = self.main.db

        # diff match patch
        self.dmp = diff_match_patch()

        # measure time
        self.times = ""

        # start the loop
        self.loop()



    def loop(self):
        while True:
            if self.main.queue_save.empty():
                #if self.data_waiting_for_commit:
                #    self.db.commit()
                #    self.data_waiting_for_commit = False

                time.sleep(1)

            else:
                self.save_item()
                #self.data_waiting_for_commit = True



    def save_item(self):
        _type, data = self.main.queue_save.get(True)

        if self.main.args.time > 0:
            self.times = ""
            start = time.time()

        if _type == "article":
            self.save_article(data)

        elif _type == "author_detail":
            self.save_author_detail(data)

        elif _type == "crawl_result":
            self.save_crawl_result(data)

        elif _type == "position":
            self.save_position(data)

        elif _type == "log":
            self.save_log(data)

        if self.main.args.time > 0:
            passed = round((time.time() - start) * 1000)

            if passed > self.main.args.time:
                print(f"\nTime to save {_type}: {passed}ms")
                print(f"Details:\n{self.times}")



    def save_log(self, data):
        self.save("log", {
            'timestamp'     : data['created'],
            'level'         : data['levelno'],
            'thread'        : data['threadName'],
            'module'        : data['module'],
            'func'          : data['funcName'],
            'line'          : data['lineno'],
            'msg'           : data['message']
        }, ignore=True)



    def save_position(self, item):
        url_id = self.save("urls", {
            "url": item["url"]
        }, sha=True)
        if not url_id:
            print("NO url_id IN save_positions")
            return False

        position = self.get("position_cache", {'url':  url_id})
        if position:
            return False

        
        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        self.save("positions", {
            'date'      : date,
            'url'       : url_id,
            'position'  : item['position']
        }, ignore=True)

        self.save("position_cache", {
            'date'      : date,
            'url'       : url_id,
            'position'  : item['position']
        }, ignore=True)



    def save_crawl_result(self, item):
        source_id           = self.save("sources", {
            "name": item["source"]
        })
        if not source_id:
            return False

        self.save("crawl_results", {
            'source'        : source_id,
            "articles_found": item['articles_found'],
            "articles_saved": item["articles_saved"]
        }, ignore=True)




    def save_author_detail(self, item):
        source_id           = self.save("sources", {
            "name": item["source"]
        })
        if not source_id:
            return False

        self.save("author_details", {
            'source'            : source_id,
            "department"        : item['department'],
            "job_description"   : item['job_desc'],
            "full_name"         : item['full_name'],
            "abbrv"             : item['abbrv']
        }, ignore=True)



    def save_article(self, item):
        # save source
        source_id           = self.save("sources", {
            "name": item["source"]
        })
        if not source_id:
            print("NO SOURCE_ID", item["source"])
            return False

        # save or update article
        url = item["url"]
        image_url = item["image_url"]
        uaid = item["internal_id"][:100]
        texts = {
            'title': item["title"][:200],
            'description': item["description"].strip(),
            'content': item["content"]
        }

        if item["date_published"] is not None:
            date_published = item["date_published"].strftime("%Y-%m-%d %H:%M:%S")
        else:
            date_published = None

        if item["date_modified"] is not None:
            date_modified = item["date_modified"].strftime("%Y-%m-%d %H:%M:%S")
        else:
            date_modified = None

        article = self.get("articles", {
            'source'        : source_id,
            'uaid'          : uaid,
        }, full=True)

        modified = 0

        # save diffs
        if article and \
        (   article['url']                 != url
            or article['image_url']        != image_url
            or article['title']            != texts['title']
            or article['description']      != texts['description']
            or article['content']          != texts['content']
            or article['date_published']   != date_published
            or article['date_modified']    != date_modified
            or article['paywall']          != item['paywall']
            or article['opinion']          != item['opinion']
        ):
            article_id = article['id']
            modified = article['modified'] + 1
            date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

            for el, text in texts.items():
                if article[el] is None or article[el] == text:
                    continue

                if text is None:
                    text = ""

                if el == "title":
                    diff = article[el]
                else:
                    diff = self.dmp.patch_toText(
                        self.dmp.patch_make(
                            text, article[el]
                        )
                    )


                self.save(
                    f"{el}s",
                    {
                        "article_id": article_id,
                        "date"      : date,
                        "text"      : diff
                    },
                    ignore=True
                )

        # save article
        if not article or modified:
            insert_article_id = self.save(
                "articles",
                {
                    'source'        : source_id,
                    'uaid'          : uaid,
                    'url'           : url,
                    'image_url'     : image_url,
                    'title'         : texts['title'],
                    'description'   : texts['description'],
                    'content'       : texts['content'],
                    'date_published': date_published,
                    'date_modified' : date_modified,
                    'paywall'       : item['paywall'],
                    'opinion'       : item['opinion']
                },
                update = {
                    'url'           : url,
                    'image_url'     : image_url,
                    'title'         : texts['title'],
                    'description'   : texts['description'],
                    'content'       : texts['content'],
                    'date_published': date_published,
                    'date_modified' : date_modified,
                    'paywall'       : item['paywall'],
                    'opinion'       : item['opinion'],
                    'modified'      : modified
                }
            )

            if insert_article_id is False:
                self.log.error("Saving article failed %s", item['url'])
                return False
                #raise ValueError("Saving article failed")

            if not article:
                article_id = insert_article_id


        # save url
        self.save(
            "urls", 
            {
                "url": url,
                "article_id": article_id
            },
            sha = True,
            update = {"article_id": article_id}
        )

        # save authors
        for author in item['authors']:
            if not isinstance(author, str):
                continue

            author_id = self.save("authors", {'name': author[:100].strip()})
            if not author_id:
                return False

            self.save('author_links', {
                'author_id' : author_id,
                'article_id': article_id
            }, ignore=True)

        # save tags
        for tag in item['tags']:
            if not isinstance(tag, str):
                continue

            tag_id = self.save("tags", {'name': tag[:50].strip()})
            if not tag_id:
                return False

            self.save('tag_links', {
                'tag_id'    : tag_id,
                'article_id': article_id
            }, ignore=True)

        # save categories
        for category in item['categories']:
            if not isinstance(category, str):
                continue

            category_id = self.save("categories", {'name': category[:50].strip()})
            if not category_id:
                return False

            self.save('category_links', {
                'category_id'   : category_id,
                'article_id'    : article_id
            }, ignore=True)



    def get(self, table, data, sha = False, full = False):
        where = []
        values = []
        wanted = "id"

        for column, value in data.items():
            if value is None:
                operator = "IS"
            else:
                operator = "="

            if sha:
                column = 'sha256'
                if value is not None:
                    value = hashlib.sha256(value.encode('utf-8')).hexdigest()
                sha = False

            where.append(f"{column} {operator} %s")
            values.append(value)

        if table == "position_cache":
            where.append("date >= (NOW() - INTERVAL %s MINUTE)")
            values.append(self.main.crawl_interval - 1)
            wanted = "url"

        where = " AND ".join(where)

        if full:
            wanted = "*"

        qry = f"SELECT {wanted} FROM {table} WHERE {where};"

        if self.main.args.time > 0:
            start = time.time()

        result = self.db.get(qry, tuple(values))

        if self.main.args.time > 0:
            passed = round((time.time() - start) * 1000)
            self.times += f"Time to fetch from {table}: {passed}ms\n"

        if not result or not result[0]:
            return False

        if full:
            return result[0]

        return result[0][wanted]



    def save(self, table, data, ignore = False, sha = False, update = False):
        if not ignore:
            _id = self.get(table, data, sha)
            if _id:
                return _id

        if table == 'articles':
            self.main.saved_articles += 1

        elif table == 'positions':
            self.main.saved_positions += 1

        elif table == 'crawl_results':
            self.main.saved_results += 1

        columns = []
        placeholders = []
        values = []

        for column, value in data.items():
            columns.append(column)
            placeholders.append("%s")
            values.append(value)

        columns = ",".join(columns)
        placeholders = ",".join(placeholders)

        ignore = "IGNORE" if ignore else ""
        qry = f"INSERT {ignore} INTO {table} ({columns}) VALUES ({placeholders})"

        if update:
            update_list = []
            for column, value in update.items():
                update_list.append(f"{column} = %s")
                values.append(value)

            update_sql = ",".join(update_list)

            qry += " ON DUPLICATE KEY UPDATE " + update_sql


        if self.main.args.time > 0:
            start = time.time()

        result = self.db.save(qry, tuple(values))

        if self.main.args.time > 0:
            passed = round((time.time() - start) * 1000)
            self.times += f"Time to save in {table}: {passed}ms\n"

        if not ignore and result is False:
            self.log.error(
                "Error Saving Data! Table: %s Data: %s\nQuery: %s",
                table,
                json.dumps(data),
                self.db.cur.mogrify(qry, tuple(values))
            )
            return False

        return result
