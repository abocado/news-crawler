#!/usr/bin/env python3
# _*_ coding:utf-8 _*_

import os
import sys
import math
from diff_match_patch import diff_match_patch
from mysql_lib import Mysql
from funcs import (
    get_config
)

# configure this
DB_OLD = "news"
DB_NEW = "news2"


# install diff-match-patch-cython
# Create news2 database with db/install.sh
# Then run this script






class Patch:
    def __init__(self):
        self.path = os.path.dirname(os.path.abspath(__file__))

        self.config = get_config(self.path)

        self.mysql_timeout = self.config.getint('mysql', 'mysql_timeout')
        self.mysql_user = self.config.get('mysql', 'mysql_user')
        self.mysql_pw = self.config.get('mysql', 'mysql_pw')
        self.mysql_host = self.config.get('mysql', 'mysql_host')

        self.db = Mysql()

        if not self.db.connect(
            self.mysql_host,
            self.mysql_user,
            self.mysql_pw,
            None,
            999999
        ):
            print("No database connection. Shutting down...")
            sys.exit(1)


    def apply(self, table):
        qry = f"TRUNCATE TABLE {DB_NEW}.{table}s;"
        self.db.get(qry)

        qry = f"USE {DB_OLD};"
        self.db.get(qry)

        qry = f"DROP TEMPORARY TABLE IF EXISTS {DB_OLD}.candidates;"
        self.db.get(qry)

        qry = f'''
            CREATE TEMPORARY TABLE
                {DB_OLD}.candidates
            SELECT
                {DB_OLD}.articles.internal_id
            FROM
                {DB_OLD}.articles
            GROUP BY
                {DB_OLD}.articles.internal_id
            HAVING
                COUNT(DISTINCT {DB_OLD}.articles.{table}) > 1;
        '''
        self.db.get(qry)


        qry = f'''
            SELECT
                {DB_OLD}.internal_ids.internal_id AS uaid,
                {DB_OLD}.articles.source AS source,
                {DB_OLD}.articles.date_downloaded AS date,
                {DB_OLD}.{table}s.{table} AS text
            FROM
                {DB_OLD}.articles
            JOIN
                {DB_OLD}.candidates 
                ON {DB_OLD}.articles.internal_id = {DB_OLD}.candidates.internal_id
            JOIN
                {DB_OLD}.{table}s 
                ON {DB_OLD}.articles.{table} = {DB_OLD}.{table}s.id
            JOIN
                {DB_OLD}.internal_ids 
                ON {DB_OLD}.articles.internal_id = {DB_OLD}.internal_ids.id
            WHERE
                {DB_OLD}.{table}s.{table} IS NOT NULL 
                AND 
                {DB_OLD}.internal_ids.internal_id IS NOT NULL
            GROUP BY
                {DB_OLD}.articles.internal_id,
                {DB_OLD}.articles.{table}
            ORDER BY
                {DB_OLD}.articles.internal_id,
                {DB_OLD}.articles.date_downloaded;
        '''

        data = self.db.get(qry)

        if not data:
            print("MYSQL ERROR")
            return False

        uaid = False
        last_text = ""
        last_date = ""

        dmp = diff_match_patch()

        row_count = len(data)
        count = 0

        for row in data:
            if uaid != row['uaid']:
                uaid = row['uaid']
            else:
                try:
                    if table == "title":
                        diff = last_text #row['text']
                    else:
                        pa = dmp.patch_make(row['text'], last_text)
                        diff = dmp.patch_toText(pa)

                #pylint: disable=broad-except
                except Exception as exc:
                    print(exc)
                    sys.exit()

                self.save(table, row['source'], uaid, last_date, diff)

            last_text = row['text']
            last_date = row['date']

            count += 1
            percent = math.ceil(count / row_count * 100)
            print(f"\rSaving {table} - Progress: {percent} %     ", end="")

        print("Finished")



    def save(self, table, source, uaid, date, diff_string):
        qry = f'''
            INSERT INTO {DB_NEW}.{table}s
                (
                    article_id,
                    date,
                    text
                )
            VALUES
                (
                    (SELECT id FROM {DB_NEW}.articles WHERE uaid = %s AND source = %s),
                    %s,
                    %s
                )
        '''

        data = (uaid, source, date, diff_string)

        if not self.db.save(qry, data):
            print("COULD NOT BE SAVED", source, uaid, diff_string)


    def transfer(self):
    # apply those one after another
        qry_list = [
            f"""
            /* COPY CONTENTS */
            INSERT INTO
                {DB_NEW}.authors
            SELECT
                *
            FROM
                {DB_OLD}.authors;
            """,f"""

            INSERT INTO
                {DB_NEW}.author_details
            SELECT
                *
            FROM
                {DB_OLD}.author_details;
            """,f"""

            INSERT INTO
                {DB_NEW}.categories
            SELECT
                *
            FROM
                {DB_OLD}.categories;
            """,f"""

            INSERT INTO
                {DB_NEW}.crawl_results
            SELECT
                *
            FROM
                {DB_OLD}.crawl_results;
            """,f"""

            INSERT INTO
                {DB_NEW}.tags
            SELECT
                *
            FROM
                {DB_OLD}.tags;
            """,f"""
            INSERT INTO
                {DB_NEW}.urls (id, url)
            SELECT
                {DB_OLD}.urls.id, {DB_OLD}.urls.url
            FROM
                {DB_OLD}.urls;
            """,f"""

            /* TRANSFER ARTICLES */
            INSERT INTO
                {DB_NEW}.articles
            SELECT
                NULL,
                {DB_OLD}.articles.source as source,
                {DB_OLD}.internal_ids.internal_id as uaid,
                {DB_OLD}.urls.url as url,
                {DB_OLD}.image_urls.image_url as image_url,
                {DB_OLD}.titles.title as title,
                {DB_OLD}.descriptions.description as description,
                {DB_OLD}.contents.content as content,
                {DB_OLD}.articles.paywall as paywall,
                {DB_OLD}.articles.opinion as opinion,
                dates_published.date as date_published,
                dates_modified.date as date_modified,
                {DB_OLD}.articles.date_downloaded as date_downloaded,
                0,
                {DB_OLD}.internal_ids.hours as hours,
                {DB_OLD}.internal_ids.average as average_position,
                {DB_OLD}.internal_ids.weight as weight,
                {DB_OLD}.internal_ids.highest_position as highest_position,
                {DB_OLD}.internal_ids.lowest_position as lowest_position,
                {DB_OLD}.internal_ids.first_seen as first_seen,
                {DB_OLD}.internal_ids.last_seen as last_seen,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
            FROM
                {DB_OLD}.articles
            JOIN
                (
                    SELECT
                        {DB_OLD}.articles.source as source,
                        {DB_OLD}.articles.internal_id as internal_id,
                        MAX({DB_OLD}.articles.date_downloaded) as date
                    FROM 
                        {DB_OLD}.articles
                    GROUP BY 
                        {DB_OLD}.articles.source,
                        {DB_OLD}.articles.internal_id
                ) AS latest_article ON
                    latest_article.source = {DB_OLD}.articles.source
                    AND
                    latest_article.internal_id = {DB_OLD}.articles.internal_id
                    AND
                    latest_article.date = {DB_OLD}.articles.date_downloaded
            LEFT JOIN
                {DB_OLD}.internal_ids 
                ON {DB_OLD}.internal_ids.id = {DB_OLD}.articles.internal_id
            LEFT JOIN
                {DB_OLD}.urls
                ON {DB_OLD}.urls.id = {DB_OLD}.articles.url
            LEFT JOIN
                {DB_OLD}.image_urls
                ON {DB_OLD}.image_urls.id = {DB_OLD}.articles.image_url
            LEFT JOIN
                {DB_OLD}.titles
                ON {DB_OLD}.titles.id = {DB_OLD}.articles.title
            LEFT JOIN
                {DB_OLD}.descriptions
                ON {DB_OLD}.descriptions.id = {DB_OLD}.articles.description
            LEFT JOIN
                {DB_OLD}.contents
                ON {DB_OLD}.contents.id = {DB_OLD}.articles.content
            LEFT JOIN
                {DB_OLD}.dates AS dates_published
                ON {DB_OLD}.articles.date_published = dates_published.id
            LEFT JOIN
                {DB_OLD}.dates AS dates_modified
                ON {DB_OLD}.articles.date_modified = dates_modified.id
            GROUP BY
                {DB_OLD}.articles.source,
                {DB_OLD}.articles.internal_id;

            """,
            f"USE {DB_NEW};",

            f"""


            /* TRANSFER TAG/AUTHOR/CATEGORY LINKS */
            CREATE TEMPORARY TABLE
                {DB_NEW}.article_list
            SELECT
                {DB_NEW}.articles.id as article_id,
                {DB_OLD}.articles.id as old_article_id,
                {DB_OLD}.articles.internal_id    
            FROM
                {DB_OLD}.articles
            JOIN
                {DB_OLD}.internal_ids ON {DB_OLD}.articles.internal_id = {DB_OLD}.internal_ids.id
            JOIN
                {DB_NEW}.articles ON {DB_NEW}.articles.uaid = {DB_OLD}.internal_ids.internal_id and {DB_NEW}.articles.source = {DB_OLD}.articles.source;
            """,f"""

            INSERT INTO
                {DB_NEW}.tag_links
            SELECT
                {DB_NEW}.article_list.article_id as article_id,
                {DB_OLD}.tag_links.tag_id
            FROM
                {DB_OLD}.tag_links
            JOIN
                {DB_NEW}.article_list
                ON {DB_NEW}.article_list.old_article_id = {DB_OLD}.tag_links.article_id
            GROUP BY
                {DB_OLD}.tag_links.tag_id,
                {DB_NEW}.article_list.internal_id;

            """,f"""


            INSERT INTO
                {DB_NEW}.category_links
            SELECT
                {DB_NEW}.article_list.article_id as article_id,
                {DB_OLD}.category_links.category_id
            FROM
                {DB_OLD}.category_links
            JOIN
                {DB_NEW}.article_list
                ON {DB_NEW}.article_list.old_article_id = {DB_OLD}.category_links.article_id
            GROUP BY
                {DB_OLD}.category_links.category_id,
                {DB_NEW}.article_list.internal_id;

            """,f"""


            INSERT INTO
                {DB_NEW}.author_links
            SELECT
                {DB_NEW}.article_list.article_id as article_id,
                {DB_OLD}.author_links.author_id
            FROM
                {DB_OLD}.author_links
            JOIN
                {DB_NEW}.article_list
                ON {DB_NEW}.article_list.old_article_id = {DB_OLD}.author_links.article_id
            GROUP BY
                {DB_OLD}.author_links.author_id,
                {DB_NEW}.article_list.internal_id;

            """,f"""


            /* TRANSFER ARTICLE IDS IN URLS */
            CREATE TEMPORARY TABLE
                {DB_NEW}.article_list2
            SELECT
                {DB_NEW}.articles.id as article_id,
                {DB_OLD}.articles.id as old_article_id,
                {DB_OLD}.articles.url as url_id,
                {DB_OLD}.articles.internal_id    
            FROM
                {DB_OLD}.articles
            JOIN
                {DB_OLD}.internal_ids ON {DB_OLD}.articles.internal_id = {DB_OLD}.internal_ids.id
            JOIN
                {DB_NEW}.articles ON {DB_NEW}.articles.uaid = {DB_OLD}.internal_ids.internal_id and {DB_NEW}.articles.source = {DB_OLD}.articles.source;
            """,f"""

            UPDATE
                {DB_NEW}.urls
            JOIN
                {DB_NEW}.article_list2
                ON {DB_NEW}.article_list2.url_id = {DB_NEW}.urls.id
            SET
                {DB_NEW}.urls.article_id = {DB_NEW}.article_list2.article_id;

            """,f"""


            /* TRANSFER POSITIONS */
            INSERT INTO
                {DB_NEW}.positions
            SELECT
                {DB_OLD}.dates.date,
                /* {DB_NEW}.urls.id as url, */
                {DB_OLD}.positions.url,
                {DB_OLD}.positions.position
            FROM
                {DB_OLD}.positions
            /*
            JOIN
                {DB_OLD}.urls ON {DB_OLD}.positions.url = {DB_OLD}.urls.id
            JOIN
                {DB_NEW}.urls ON {DB_OLD}.urls.sha256 = {DB_NEW}.urls.sha256
            */
            JOIN
                {DB_OLD}.dates ON {DB_OLD}.positions.date = {DB_OLD}.dates.id;

            """,f"""

            /* TRANSFER POSITION_CACHE */
            INSERT INTO
                {DB_NEW}.position_cache
            SELECT
                {DB_OLD}.dates.date,
                /* {DB_NEW}.urls.id as url, */
                {DB_OLD}.position_cache.url,
                {DB_OLD}.position_cache.position,
                {DB_OLD}.position_cache.analyzed
            FROM
                {DB_OLD}.position_cache
            JOIN
                {DB_OLD}.urls ON {DB_OLD}.position_cache.url = {DB_OLD}.urls.id
            JOIN
                {DB_NEW}.urls ON {DB_OLD}.urls.sha256 = {DB_NEW}.urls.sha256
            JOIN
                {DB_OLD}.dates ON {DB_OLD}.position_cache.date = {DB_OLD}.dates.id;
            """
        ]

        for qry in qry_list:
            self.db.get(qry)

        print("Transfer complete")






patch = Patch()
patch.transfer()

tables = ["content", "title" , "description"]
for t in tables:
    patch.apply(t)
