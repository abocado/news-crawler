#!/usr/bin/env python3
# _*_ coding:utf-8 _*_
#pylint: disable=invalid-name

import os
import sys
import logging
import importlib
import signal
import time
from datetime import datetime
from threading import Thread
from queue import Queue
from funcs import (
    setup_logger,
    get_config,
    get_requests_per_min,
    get_exec_time,
    setup_arg_parser
)
from mysql_lib import Mysql


# version and dependency checks
if sys.version_info < (3,7,0):
    print("Python 3.7 or higher is required!")



class NewsCrawler:
    #pylint: disable=redefined-outer-name
    def __init__(self, args):
        # get current dir
        self.path = os.path.dirname(os.path.abspath(__file__))

        # initiate queues
        self.queue_save = Queue()
        self.queue_request = Queue()

        # declarations
        self.threads = []
        self.dead_threads = 0
        self.modules = {}
        self.saved_articles = 0
        self.saved_positions = 0
        self.saved_results = 0
        self.saved_groups = 0
        self.requests = 0
        self.requests_avg = {}
        self.requests_per_min = 0
        self.save_threads = 0
        self.group_threads = 0
        self.request_threads = 0
        self.site_threads = 0
        self.start_time = datetime.now()
        self.server_online = False
        self.gb_downloaded = 0
        self.wanted_site_threads = 0
        self.json_status_lock = 0

        # disable traceback on ctrl + c
        signal.signal(signal.SIGINT, lambda x, y: sys.exit(0))
        #signal.signal(signal.SIGINT, self.on_exit)

        # arg parse
        self.args = args
        self.args.time = int(self.args.time)

        # load options from config
        self.config = get_config(self.path)
        self.wanted_request_threads = self.config.getint('settings', 'request_threads', fallback=4)
        self.crawl_interval = self.config.getint('settings', 'crawl_interval', fallback=60) #minutes
        self.request_queue_limit = self.config.getint('settings', 'request_queue_limit',
                                                        fallback=1000)
        self.save_queue_limit = self.config.getint('settings', 'save_queue_limit', fallback=1000)

        self.article_grouping = self.config.getboolean('grouping', 'article_grouping',
                                                        fallback=False)
        self.stop_word_list = self.config.get('grouping', 'stop_word_list',
                                                        fallback="")
        self.compound_split_list = self.config.get('grouping', 'compound_split_list',
                                                        fallback="")

        self.mysql_timeout = self.config.getint('mysql', 'mysql_timeout')
        self.mysql_user = self.config.get('mysql', 'mysql_user')
        self.mysql_pw = self.config.get('mysql', 'mysql_pw')
        self.mysql_db = self.config.get('mysql', 'mysql_db')
        self.mysql_host = self.config.get('mysql', 'mysql_host')

        if self.args.quiet:
            logging_level = logging.ERROR
        elif self.args.site:
            logging_level = logging.DEBUG
        else:
            logging_level = self.config.getint('settings', 'logging_level', fallback=0)

        # logger
        self.log = setup_logger(logging_level, self.queue_save)

        self.log.info("News Crawler started!")


        # connect to mysql server
        self.db = Mysql()

        if not self.db.connect(
            self.mysql_host,
            self.mysql_user,
            self.mysql_pw,
            self.mysql_db,
            self.mysql_timeout
        ):
            self.log.error("No database connection. Shutting down...")
            sys.exit(1)

        # get known urls of last two intervals to avoid crawling too often after restart
        if not self.args.article and not self.args.landing:
            self.known_urls = self.get_known_urls()
        else:
            self.known_urls = []

        # load and watch
        self.load_threads()

        self.loop()



    def load_threads(self):
        self.load_thread('save')
        self.load_request_threads()
        self.load_site_threads()

        if self.args.json:
            self.load_thread('server')

        if self.article_grouping:
            self.load_thread('grouping')

        # start all threads
        for t in self.threads:
            t.start()



    def load_site_threads(self):
        sites = os.path.join(self.path, "sites")

        # import all site specific scripts and initiate their threads
        for site_path in os.listdir(sites):
            full_path = os.path.join(sites, site_path)

            if not os.path.isfile(full_path) \
            or not site_path.endswith(".py") \
            or site_path.startswith("__"):
                continue

            if self.args.site is not None \
            and not site_path.startswith(self.args.site):
                continue

            site = os.path.splitext(site_path)[0]

            self.wanted_site_threads += 1
            self.load_thread(site)



    def load_request_threads(self):
        # initiate worker that requests content from urls and passes them to a callback
        # pylint: disable=unused-variable
        for thread_nr in range(self.wanted_request_threads):
            self.load_thread('request')



    def import_module(self, name):
        if name in sys.modules:
            return sys.modules[name]

        return importlib.import_module(name)



    def load_thread(self, name):
        if name in ['save', 'request', 'server', 'grouping']:
            module = self.import_module(name)
            class_name = name.title()
            thread_args = [self]

        else:
            module = self.import_module(f"sites.{name}")
            class_name = 'Crawler'
            landing_page = getattr(module, 'LandingPage')
            article = getattr(module, "Article")
            thread_args = [self, landing_page, article]

        func = getattr(module, class_name)

        self.modules[name] = module

        self.threads.append(
            Thread(
                target = func,
                args = thread_args,
                daemon = True,
                name = name
            )
        )



    def count_threads(self):
        self.group_threads = 0
        self.save_threads = 0
        self.request_threads = 0
        self.site_threads = 0
        self.server_online = False

        for thread in self.threads[:]:
            if not thread.is_alive():
                self.log.error("%s thread died!", thread.name)
                self.dead_threads += 1
                self.threads.remove(thread)

                #pylint: disable=pointless-string-statement
                ''' maybe useful one time for reloading dead threads at runtime
                self.log.info("Reloading module %s", thread.name)

                self.threads.remove(thread)
                self.dead_threads.remove(thread.name)

                # reload thread
                self.load_thread(thread.name)

                self.threads[-1].start()
                '''

            else:
                if thread.name == 'save':
                    self.save_threads += 1
                elif thread.name == 'request':
                    self.request_threads += 1
                elif thread.name == 'server':
                    self.server_online = True
                elif thread.name == 'grouping':
                    self.group_threads += 1
                else:
                    self.site_threads += 1



    # on start up, get the urls of the recent crawls (if any, crawl_interval * 2)
    # to avoid crawling urls too often
    # if --rescan argument is set, it takes the urls since specified date
    def get_known_urls(self):
        if self.args.rescan:
            qry = """
                SELECT 
                    articles.url,
                    sources.name
                FROM
                    articles
                JOIN
                    sources
                    ON sources.id = articles.source
                WHERE 
                    articles.date_downloaded >= %s
                GROUP BY
                    articles.url
            """

            data = [self.args.rescan]

        else:
            qry = """
                SELECT 
                    articles.url,
                    sources.name
                FROM 
                    position_cache
                JOIN
                    urls
                    ON urls.id = position_cache.url
                JOIN
                    articles
                    ON articles.id = urls.article_id
                JOIN
                    sources
                    ON sources.id = articles.source
                WHERE 
                    position_cache.date >= (NOW() - INTERVAL %s MINUTE)
                GROUP BY
                    position_cache.url
            """

            data = [self.crawl_interval * 2]

        result = self.db.get(qry, data)

        known_urls = {}

        if not result:
            return known_urls

        for item in result:
            if item['name'] not in known_urls:
                known_urls[item['name']] = []

            known_urls[item['name']].append(item['url'])

        return known_urls




    def loop(self):
        count = 10

        # loop
        while True:
            now = datetime.now()

            if count >= 10:
                self.count_threads()
                count = 0

            count += 1

            self.requests_per_min = get_requests_per_min(self.requests_avg, self.requests, now)

            if not self.args.quiet:
                self.print_status(now)

            time.sleep(0.3)




    def print_status(self, now):
        if now.second < 50:
            print(
                f"\rSaved articles {self.saved_articles}" \
                f" | Saved positions {self.saved_positions}" \
                f" | Saved results {self.saved_results}" \
                f" | Save queue {self.queue_save.qsize()}" \
                f" | Request queue {self.queue_request.qsize()}" \
                f" | Requests / minute {self.requests_per_min}" \
                f" | Requested {self.requests} URLs          ",
                end=""
            )

        else:
            print(
                f"\rSite thread(s) {self.site_threads}" \
                f" | Request thread(s) {self.request_threads}" \
                f" | Save thread(s) {self.save_threads}" \
                f" | Dead thread(s) {self.dead_threads}" \
                f" | Execution time {get_exec_time(self.start_time, now)}" \
                f" | Traffic {self.gb_downloaded:.3f} GB                    ",
                end=""
            )



    def print_json_status(self):
        if self.json_status_lock > 10:
            return False

        if self.json_status_lock > 0:
            time.sleep(0.1)
            return self.print_json_status()

        self.json_status_lock += 1

        group_threads_expected = '1' if self.article_grouping else '0'

        json = {
            "Articles saved"    : self.saved_articles,
            "Positions saved"   : self.saved_positions,
            "Results saved"     : self.saved_results,
            "Groups saved"      : self.saved_groups,
            "Save Queue"        : f"{self.queue_save.qsize()} / {self.save_queue_limit}",
            "Request Queue"     : f"{self.queue_request.qsize()} / {self.request_queue_limit}",
            "Requests / minute" : self.requests_per_min,
            "Total Requests"    : self.requests,
            "Traffic"           : f"{self.gb_downloaded:.3f} GB",
            "Site Threads"      : f"{self.site_threads} / {self.wanted_site_threads}",
            "Request Threads"   : f"{self.request_threads} / {self.wanted_request_threads}",
            "Save Threads"      : f"{self.save_threads} / 1",
            "Group Threads"     : f"{self.group_threads} / {group_threads_expected}",
            "Dead Threads"      : self.dead_threads,
            "Execution Time"    : self.start_time.timestamp()
        }

        self.json_status_lock -= 1

        return json



    #pylint: disable=unused-argument
    def on_exit(self, x, y):
        print(f"\n\nRequest queue length: {self.queue_request.qsize()}\n\nRemaining queue items:")

        while not self.queue_request.empty():
            print(self.queue_request.get(True)[2])

        sys.exit(1)



if __name__ == "__main__":
    arguments = setup_arg_parser()

    if arguments.daemon:
        #pylint: disable=bare-except, import-error
        try:
            import daemon
        except:
            print("install 'daemon' package first!")
            sys.exit()

        arguments.quiet = True

        with daemon.DaemonContext():
            NewsCrawler(arguments)

    else:
        NewsCrawler(arguments)
