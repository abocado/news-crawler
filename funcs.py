import logging
import logging.handlers
import os
import sys
import urllib.request
import urllib.parse
import urllib.error
from time import sleep
import configparser
import json
import argparse
import random
import re

import lxml.html
import lxml.etree



def get_url_content(url, user_agents = None, post = None):
    max_retries = 10
    retries = 0

    request = False

    post_data = None
    if post:
        post_data = urllib.parse.urlencode(post).encode()

    # if user_agents is specified, a random user_agent from the list is picked
    headers = {}
    if user_agents is not None:
        headers['User-Agent'] = random.choice(user_agents)

    url = urllib.request.Request(
        urllib.parse.quote(url, safe=":/?="),
        headers=headers,
        data=post_data
    )

    while True:
        #pylint: disable=broad-except
        try:
            with urllib.request.urlopen(url, timeout=60) as request:
                content = request.read().decode("utf8", errors='replace')
                request.close()
                break

        except urllib.error.HTTPError as err:
            print("HTTPError:", err)
            return False

        # urllib.error.URLError
        except Exception as exc:
            retries += 1
            if retries > max_retries:
                print('Retry Error', type(exc), exc)
                return False

            sleep(60)

    return content



def parse_content(content, xml = False):
    #pylint: disable=c-extension-no-member
    if xml:
        return lxml.etree.fromstring(content.encode('utf8'))

    return lxml.html.fromstring(content)




def parse_file(file, xml = False):
    #pylint: disable=c-extension-no-member
    if xml:
        return lxml.etree.parse(file)

    return lxml.html.parse(file)



class QueueHandler(logging.handlers.QueueHandler):
    def enqueue(self, record):
        self.queue.put(["log", record.__dict__])



def setup_logger(logging_level, queue):
    log = logging.getLogger(__name__)
    log.setLevel(logging.DEBUG)

    # output logs to stdout
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging_level)
    log.addHandler(handler)

    # send logs to database
    handler = QueueHandler(queue)
    handler.setLevel(logging.DEBUG)
    log.addHandler(handler)

    return log



def get_config(path):
    config_path = os.path.join(path, 'config.ini')
    config = configparser.ConfigParser(allow_no_value=True, comment_prefixes="/")

    try:
        config.read(config_path)
    except configparser.Error:
        pass

    if not config.has_section('settings'):
        config.add_section('settings')

    if not config.has_option('settings', 'crawl_interval'):
        config.set('settings', '# interval to start the crawling in minutes', None)
        config.set('settings', 'crawl_interval', '60')

    if not config.has_option('settings', 'logging_level'):
        config.set('settings', 'logging_level', '20')

    if not config.has_option('settings', 'request_threads'):
        config.set('settings', 'request_threads', '8')

    if not config.has_option('settings', 'request_queue_limit'):
        config.set('settings', 'request_queue_limit', '1000')

    if not config.has_option('settings', 'save_queue_limit'):
        config.set('settings', 'save_queue_limit', '1000')

    if not config.has_option('settings', 'article_grouping'):
        config.set('settings', 'article_grouping', 'false')

    if not config.has_section('grouping'):
        config.add_section('grouping')

    if not config.has_option('grouping', 'article_grouping'):
        config.set('grouping', 'article_grouping', 'false')

    if not config.has_option('grouping', 'stop_word_list'):
        config.set('grouping', 'stop_word_list', '')

    if not config.has_option('grouping', 'compound_split_list'):
        config.set('grouping', 'compound_split_list', '')

    no_mysql = False

    if not config.has_section('mysql'):
        no_mysql = True
        config.add_section('mysql')

    if not config.has_option('mysql', 'mysql_host'):
        config.set('mysql', 'mysql_host', "")
    if not config.has_option('mysql', 'mysql_user'):
        config.set('mysql', 'mysql_user', "")
    if not config.has_option('mysql', 'mysql_pw'):
        config.set('mysql', 'mysql_pw', "")
    if not config.has_option('mysql', 'mysql_db'):
        config.set('mysql', 'mysql_db', "")
    if not config.has_option('mysql', 'mysql_timeout'):
        config.set('mysql', 'mysql_timeout', "20")


    with open(config_path, 'w', encoding="utf-8") as configfile:
        config.write(configfile)

    if no_mysql:
        print("No MySQL configuration found! Please edit config.ini.")
        sys.exit(0)

    return config



def get_ld_json(root):
    json_raw = root.xpath('.//script[@type="application/ld+json"]/text()')

    if not json_raw:
        return False

    result = {}

    for json_string in json_raw:
        json_string = json_string\
            .replace("\n", " ")\
            .replace("\r", " ")

        #pylint: disable=bare-except, broad-except
        try:
            json_data = json.loads(json_string)
        except Exception as err:
            print("JSON COULD NOT BE PARSED", err)
            print(f"JSON STRING >>>{json_string}<<<")
            return False

        if not json_data:
            continue

        if not isinstance(json_data, list):
            if "@graph" in json_data:
                json_data = json_data['@graph']
            else:
                json_data = [json_data]

        for data in json_data:
            if "@type" not in data or isinstance(data["@type"], list):
                continue

            result[data['@type']] = data

    return result



def get_requests_per_min(data, current, now):
    now = now.timestamp()

    if not data:
        data[now] = 0
        return 0

    data[now] = current

    oldest = now

    data_copy = data.copy()

    for stamp in data_copy:
        # remove entries older than 60 seconds
        if stamp < (now - 60):
            del data[stamp]
            continue

        if stamp < oldest:
            oldest = stamp

    return current - data_copy[oldest]



def get_exec_time(start, now):
    elapsed = now - start
    hours = elapsed.seconds // 3600
    minutes = elapsed.seconds // 60 % 60
    seconds = elapsed.seconds % 60
    return f"{elapsed.days}:{hours:02d}:{minutes:02d}:{seconds:02d}"



def setup_arg_parser():
    parser = argparse.ArgumentParser(
        description = 'A crawler for the news'
    )

    parser.add_argument(
        '-s', '--site',
        metavar="<site>",
        help="Crawl only specific site."
    )

    parser.add_argument(
        '-l', '--landing',
        action="store_true",
        help="Print links found on landing page"
    )

    parser.add_argument(
        '-a', '--article',
        action="store_true",
        help="Print article data found"
    )

    parser.add_argument(
        '-q', '--quiet',
        action="store_true",
        help="No output, except errors"
    )

    parser.add_argument(
        '-j', '--json',
        action="store_true",
        help="Enable JSON Server (Default Port 8080)"
    )

    parser.add_argument(
        '-p', '--port',
        metavar="<port>",
        help="Set port for the JSON Server",
        default=8080
    )

    parser.add_argument(
        '-d', '--daemon',
        action="store_true",
        help="Daemonize (run in background)"
    )

    parser.add_argument(
        '-r', '--rescan',
        metavar="<YYYY-MM-DD>",
        help="Rescan articles since specified date until now"
    )

    parser.add_argument(
        '-t', '--time',
        metavar="<miliseconds>",
        help="Measure time needed to save items in database.\n"\
             + "Measurements are printed only if above specified miliseconds",
        default=0
    )

    parser.add_argument(
        '--url',
        metavar="<url>",
        help="(Re)scan given URL"
    )

    return parser.parse_args()



def filter_authors(authors):
    authors_filtered = []

    for author in authors:
        if not author:
            continue

        author_split = re.split(r' UND | Und | u. | & | und |\/|, |,', author)

        for author_sub in author_split:
            if not author_sub:
                continue

            if "(" in author_sub:
                author_sub = author_sub[:author_sub.rfind("(")]

            author_sub = re.sub(
                r'([Mm]it (?:Material|Informationen) (?:von|der))',
                '',
                author_sub
            )

            author_sub = author_sub.strip()

            if len(author_sub.split()) > 1:
                author_sub = author_sub.title()

            if author_sub in authors_filtered or not author_sub:
                continue

            authors_filtered.append(author_sub)

    return authors_filtered
