import json
from time import sleep
from funcs import get_url_content


class Request:
    def __init__(self, main):
        self.main = main

        self.loop()



    def loop(self):
        while True:
            if self.main.queue_request.empty() \
            or self.main.queue_save.qsize() > self.main.save_queue_limit:
                sleep(1)
            else:
                self.request()



    def request(self):
        self.main.requests += 1

        callback, args, url, user_agents, *json_redirect = self.main.queue_request.get(True)

        content = get_url_content(url, user_agents=user_agents)

        if json_redirect and content and len(content) < 300:
            json_data = json.loads(content)
            if 'type' in json_data and json_data['type'] == 'redirect':
                new_location = url[:url.find('?url=')+5] + json_data['location']
                content = get_url_content(new_location, user_agents=user_agents)

        if not content:
            self.main.log.warning(
                "No connection could be established to %s",
                url
            )
            return

        self.main.gb_downloaded += len(content) / 1024 / 1024 / 1024

        #pylint: disable=broad-except
        try:
            callback(content, *args)
        except Exception as err:
            self.main.log.error("ERROR URL: %s", url)
            self.main.log.exception("%s %s", err, type(err))
