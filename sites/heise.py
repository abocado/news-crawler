from datetime import datetime
import re

from funcs import get_url_content, parse_content
from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Heise"
        self.base_url = \
        self.landing_page = "https://www.heise.de"



    def get_author_detail(self):
        raw_content = get_url_content('https://www.heise.de/impressum.html')
        if not raw_content:
            return False

        parsed_content = parse_content(raw_content)

        paragraphs = parsed_content\
            .xpath('.//div[@class="verantwortlich"]/p')

        department = ''

        for p in paragraphs:
            children = p.getchildren()
            if children is None or len(children) < 1:
                continue

            if children[0].tag != "strong":
                continue

            job_desc = children[0].text_content().strip()[:-1].strip()

            authors = p.xpath('./a[contains(@href, "mailto:")]')

            for author in authors:
                href = author.attrib["href"]
                if len(href) < 18:
                    continue

                abbrv = href[7:-9]

                if len(abbrv) not in [2,3,4]:
                    continue

                full_name = author.text_content()

                data = {
                    "type": "author_detail",
                    "data": {
                        "source"    : self.name,
                        "department": department,
                        "job_desc"  : job_desc,
                        "full_name" : full_name,
                        "abbrv"     : abbrv
                    }
                }

                self.main.queue_save.put(data, True)




class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//article//a")



    def verify(self, article):
        if not article.attrib['href'].endswith(".html"):
            return False

        if self.url.startswith(self.base_url + '/tp/'):
            return False

        return True



    def get_url(self, article):
        return article.attrib['href'] + "?seite=all"



class Article(ArticleBase):
    def get_tags(self):
        tags = []

        if not self.json or not self.json["NewsArticle"]["keywords"]:
            return []

        tag_list = re.split(r'\/|, |,', self.json["NewsArticle"]["keywords"])

        for tag in tag_list:
            tags.append(tag.strip())

        return tags



    def get_date_published(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]['datePublished']
        ).replace(tzinfo=None)



    def get_date_modified(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]['dateModified']
        ).replace(tzinfo=None)



    def get_authors(self):
        authors = []

        author_list = self.json["NewsArticle"]["author"]
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            if not author["name"]:
                continue
            authors.append(author["name"])

        return authors



    def is_paywall(self):
        if isinstance(self.json["NewsArticle"]["isAccessibleForFree"], str):
            return not self.json["NewsArticle"]["isAccessibleForFree"].lower() == "true"

        return not self.json["NewsArticle"]["isAccessibleForFree"]



    def get_categories(self):
        categories = self.content\
            .xpath('.//a[@class="navigation__head  navigation__head--active"]/text()')

        if self.json["NewsArticle"]["keywords"] == "Auto":
            categories.append("Auto")

        category_breadcrumb = self.content\
            .xpath('.//ul[@class="a-breadcrumb__crumbs"]/li[@class="a-breadcrumb__crumb"][2]//a/text()')

        if category_breadcrumb:
            categories.append(category_breadcrumb[0].replace("\n", "").strip())

        if '/en/news/' in self.url:
            categories.append("English")

        # remove duplicates
        return list(filter(len, set(categories)))



    def get_internal_id(self):
        return self.url[self.url.rfind('-')+1:self.url.rfind('.html')]



    def get_content(self):
        content = ''
        paragraphs = self.content\
            .xpath('.//article//div[contains(@class, "article-content")]/p')
        if len(paragraphs) >= 2:
            paragraphs.pop() #remove the last p because it only contains the authors tag

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        return content
