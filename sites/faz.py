from datetime import datetime
import re
import locale
from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase
import lxml.etree


class Crawler(CrawlerBase):
    def init(self):
        self.name = "FAZ"
        self.base_url = "https://www.faz.net"
        self.landing_page = "https://www.faz.net/aktuell/"



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath('//main//a[@href]')



    def verify(self, article):
        if not article.attrib['href'].startswith(self.base_url):
            return False

        if not article.attrib['href'].endswith(".html"):
            return False

        blacklist = [
            '/allesbeste/',
            '/cartoons/',
            '/stil/',
            '/technik-motor/',
            '/bundesliga/live/',
            '/spiele/',
            '/kaufkompass/'
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                return False

        return True



class Article(ArticleBase):
    def parse_json(self):
        mandatory = {
            "NewsArticle": [
                'dateModified'
            ],
            "BreadcrumbList": [
                'itemListElement'
            ]
        }

        if not self._parse_json(mandatory):
            return False

        locale.setlocale(locale.LC_TIME, "en_US.UTF-8")

        return True


    def get_title(self):
        title_el = self.content.xpath('.//meta[@property="og:title"]')
        if title_el:
            return title_el[0].attrib["content"].strip()

        if "name" in self.json["NewsArticle"]:
            return self.json["NewsArticle"]["name"]

        return None



    def get_tags(self):
        tags = self.content.xpath('.//main/article//div[contains(@class,"footer-links")]/a/text()')

        if not tags:
            return []

        if "Alle Themen" in tags: tags.remove("Alle Themen")

        return tags



    def get_date_published(self):
        # Tue Nov 01 10:20:48 CET 2022
        date = self.json["NewsArticle"]['datePublished']
        date_published = date.split(" ")
        if len(date_published) < 4:
            return datetime.fromisoformat(
                date[:date.rfind("+")]
            ).replace(tzinfo=None)

        del date_published[4]
        return datetime.strptime(" ".join(date_published), "%a %b %d %H:%M:%S %Y")



    def get_date_modified(self):
        # Tue Nov 01 10:20:48 CET 2022
        date = self.json["NewsArticle"]['dateModified']
        date_modified = date.split(" ")
        if len(date_modified) < 4:
            return datetime.fromisoformat(
                date[:date.rfind("+")]
            ).replace(tzinfo=None)

        del date_modified[4]
        return datetime.strptime(" ".join(date_modified), "%a %b %d %H:%M:%S %Y")



    def get_authors(self):
        authors = []

        blacklist = [
            "FAZ.NET",
            "Frankfurter Allgemeine Zeitung"
        ]

        if 'author' not in self.json["NewsArticle"]:
            return authors

        author_list = self.json["NewsArticle"]["author"]
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            author_split = re.split(r' und | u. | & |\/|, |,', author["name"])

            for a in author_split:
                if a in blacklist:
                    continue

                authors.append(a.strip())

        return authors



    def is_paywall(self):
        if 'isAccessibleForFree' not in self.json["NewsArticle"]:
            return False

        return not self.json["NewsArticle"]['isAccessibleForFree']



    def get_categories(self):
        categories = []
        category_list = self.json["BreadcrumbList"]['itemListElement'][1:-1] #remove first and last

        for category in category_list:
            categories.append(category["name"])


        return categories



    def get_content(self):
        if not "articleBody" in self.json["NewsArticle"]:
            self.parent.log.log(1,"\nFAZ.NET: NO ARTICLEBODY %s", self.url)
            return None

        return self.json["NewsArticle"]['articleBody'].replace("\xc2\xad", "")



    def get_internal_id(self):
        if not self.url.endswith(".html"):
            return None

        return self.url[self.url.rfind('-')+1:-5]



    def is_opinion(self):
        opinion = self.content.xpath('.//div[contains(@class,"header-detail--opinion")  ]')
        if isinstance(opinion, list) and len(opinion) > 0:
            return True

        return False
