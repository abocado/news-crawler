import re
from datetime import datetime
from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "SZ"
        self.base_url = \
        self.landing_page = "https://www.sueddeutsche.de"




class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath('//article//a[@data-manual]')



    def verify(self, article):
        blacklist = [
            '/video/',
            '/sport-liveticker/',
            '/raetsel/',
            '/spiele/',
            'dpa.urn-newsml-dpa-com'
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                return False

        #return article.attrib['href'][-5:-1].isnumeric()
        #return len(article.attrib['href'][article.attrib['href'].rfind('.')+1:]) == 22
        return article.attrib['href'][article.attrib['href'].rfind('.')+1:].isnumeric()




class Article(ArticleBase):
    def get_tags(self):
        tags = []
        tag_list = self.content.xpath('.//meta[@name="keywords"]')\
            [0].attrib["content"].split(",")

        for tag in tag_list:
            tag = tag.strip()
            if tag != 'SZ' and tag != 'Süddeutsche Zeitung':
                tags.append(tag)

        return tags



    def get_date_published(self):
        date = self.json["NewsArticle"]['datePublished']

        if date.endswith("Z"):
            date = date[:-5] + "+02:00"
        elif date[-5] == "+":
            date = date[:-2] + ":00"

        return datetime.fromisoformat(
            date
        ).replace(tzinfo=None)



    def get_date_modified(self):
        date = self.json["NewsArticle"]['dateModified']

        if date.endswith("Z"):
            date = date[:-5] + "+02:00"
        elif date[-5] == "+":
            date = date[:-2] + ":00"

        return datetime.fromisoformat(
            date
        ).replace(tzinfo=None)



    def get_authors(self):
        blacklist = ["SZ", "Süddeutsche Zeitung", "Süddeutsche.de GmbH, Munich, Germany"]

        authors = []
        author_list = self.content.xpath('.//meta[@name="author"]')

        for author in author_list:
            if author.attrib['content'] in blacklist:
                continue
            authors.append(author.attrib['content'])

        author_short_el = self.content.xpath('.//small[@class="css-opzsry"]/text()')
        if author_short_el:
            author_shorts = re.split(r'und|\/|, |,', author_short_el[0])
            for author in author_shorts:
                if author in blacklist:
                    continue

                authors.append(author)

        author_alternative_el = self.content.xpath('.//p[@class="css-1dqanco" or @class="css-2udw1p"]/text()')
        if len(author_alternative_el) == 0:
            author_alternative_el = self.content.xpath('.//div[contains(@class, "author")]/text()')

        if author_alternative_el:
            author_alternative = re.split(r'und|\/|, |,', author_alternative_el[0].replace("Von", ""))
            for author in author_alternative:
                if author in blacklist:
                    continue

                authors.append(author.strip())


        author_dpa_1 = self.content.xpath('.//p[@class="css-1nw5r4g"]/text()')
        if len(author_dpa_1) > 0 and 'dpa-Newskanal' in author_dpa_1[0]:
            authors.append("dpa")

        author_dpa_2 = self.content.xpath("(.//p[contains(@class, 'css-13wylk3')]/text())[last()]")
        if len(author_dpa_2) > 0 and 'dpa-infocom' in author_dpa_2[0]:
            authors.append("dpa")


        authors_cleaned = []
        for author in authors:
            author = author.strip()
            
            if "(" in author and author.endswith(")"):
                author = author[author.rfind("(")+1:-1]

            if author in authors_cleaned or author == '' or not author:
                continue

            authors_cleaned.append(author)

        # remove dupes from authors
        return authors_cleaned



    def is_paywall(self):
        if "isAccessibleForFree" in self.json["NewsArticle"]:
            return self.json["NewsArticle"]["isAccessibleForFree"] == "False"
        else:
            return False



    def get_categories(self):
        categories1 = []
        if 'articleSection' in self.json["NewsArticle"]:
            categories1 = [self.json["NewsArticle"]['articleSection']]

        categories2_el = self.content\
            .xpath('.//nav[@id="contextNavigation"]/ol/li/a/span/text()')

        if not categories2_el or len(categories2_el) < 3:
            return categories1

        categories2 = []
        for category in categories2_el[1:-1]:
            categories2.append(category.strip())

        return list(set(categories1 + categories2))



    def get_internal_id(self):
        internal_id = self.content.xpath('.//meta[@property="sz:primaryId"]')
        if internal_id:
            internal_id[0].attrib["content"].strip()

        internal_id = self.content.xpath('.//meta[@name="cXenseParse:articleid"]')
        if internal_id:
            internal_id[0].attrib["content"].strip()

        internal_id = self.url[self.url.rfind('.')+1:]
        if internal_id.endswith("/"):
            internal_id = internal_id[:-1]

        if len(internal_id) < 1:
            return None

        return internal_id



    def get_content(self):
        if "articleBody" in self.json["NewsArticle"]:
            return self.json["NewsArticle"]['articleBody']

        return ""
