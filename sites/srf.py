from datetime import datetime

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase
from funcs import filter_authors

import json


class Crawler(CrawlerBase):
    def init(self):
        self.name = "SRF"
        self.base_url = "https://www.srf.ch/"
        self.landing_page = "https://www.srf.ch/news"



class LandingPage(LandingPageBase):
    def verify(self, article):
        blacklist = [
            'sport',
            'radio',
            'kultur',
            'in-eigener-sache'
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                return False

        return True


    def get_article_list(self): 
        return self.content.xpath(".//a[contains(@class,'teaser-ng--article')]")
       

    def get_url(self, article):
        return article.attrib['href']



class Article(ArticleBase):
    def parse_json(self):
        data_json = self.content.xpath(".//span[contains(@id,'config__js')]/@data-analytics-udp-value-object")

        if data_json:
            self.json = json.loads(data_json[0])
            self.json['opinion'] = False
            return True
        
        self.parent.log.debug("No JSON Data found! Skipping: %s", self.url)
        return False


    def get_date_published(self):
         return datetime.fromisoformat(self.json["content_publication_datetime"]).replace(tzinfo=None)


    def get_date_modified(self):
        return datetime.fromisoformat(self.json["content_modification_datetime"]).replace(tzinfo=None)


    def get_authors(self):
        authors = []

        author_shorts = self.content.xpath(".//span[contains(@itemprop,'author')]/span/text()")
        for author in author_shorts:
            author_split = author.split(";")
            for author2 in author_split:
                if author2.startswith("Eine Analyse von "):
                    author2 = author2.replace("Eine Analyse von ", "")
                    self.json['opinion'] = True
                authors.append(author2.strip())

        
        author_list = self.content.xpath(".//p[contains(@class,'person-details__name')]/text()")
        for author in author_list:
            authors.append(author.strip())

        authors2 = self.content.xpath(".//span[contains(@itemprop,'author')]/text()")
        for author in authors2:
            authors.append(author.strip())

        return filter_authors(authors)


    def is_paywall(self):
        return False


    def get_categories(self):
        categories = []
        if self.json["content_category_1"] : 
            categories.append(self.json["content_category_1"])
        if self.json["content_category_2"] : 
            categories.append(self.json["content_category_2"])
        if self.json["content_category_3"] : 
            categories.append(self.json["content_category_3"])
        if self.json["content_category_4"] : 
            categories.append(self.json["content_category_4"])
        return categories


    def get_tags(self):
    #   no tags ?!
        return []


    def get_internal_id(self):
        internal_id = self.content.xpath('.//meta[@name="srf:content:id"]')
        if not internal_id:
            return None

        return internal_id[0].attrib["content"].strip()


    def get_content(self):
        content = ''
        elements = self.content.xpath('//section[contains(@class,"article-content")]//*[self::p or self::h1 or self::h2 or self::h3 or self::h4 or self::h5 or self::h6]')
        for element in elements:
            text = element.xpath('string()').strip()
            if text:
                content += text
        return content


    def is_opinion(self):
        return self.json['opinion']
