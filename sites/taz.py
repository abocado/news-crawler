import re
from datetime import datetime
from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "taz"
        self.base_url = \
        self.landing_page = "https://www.taz.de"



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//main//a[contains(@class, 'teaser-link')]")




class Article(ArticleBase):
    def parse_json(self):
        mandatory = {
            "NewsArticle": [
            ],
            "BreadcrumbList": [
                'itemListElement'
            ]
        }

        if not self._parse_json(mandatory):
            return False

        return True


    def get_permalink(self):
        return self.content.xpath('.//meta[@property="og:url"]')[0].attrib["content"].strip()



    def get_tags(self):
        tags = []
        tag_list = self.content.xpath('.//meta[@name="taz:tag"]')

        for tag in tag_list:
            tag = tag.attrib["content"].strip()
            if tag != 'taz' and tag != 'tageszeitung':
                tags.append(tag)

        return tags



    def get_date_published(self):
        date_published_el = self.content \
            .xpath('.//meta[@property="article:published_time"]')[0].attrib["content"].strip()

        return datetime.fromisoformat(
            date_published_el
        ).replace(tzinfo=None)



    def get_date_modified(self):
        date_modified_el = self.content \
            .xpath('.//meta[@property="article:modified_time"]')

        if not date_modified_el:
            return None

        return datetime.fromisoformat(
            date_modified_el[0].attrib["content"].strip()
        ).replace(tzinfo=None)



    def get_authors(self):
        if 'author' not in self.json["NewsArticle"]:
            return []

        authors = []
        for a in self.json["NewsArticle"]["author"]:
            if "name" in a:
                authors.append(a["name"].strip())
        return authors



    def is_paywall(self):
        return not self.json["NewsArticle"]["isAccessibleForFree"].lower() == "true"



    def get_categories(self):
        categories = []
        category_list = self.json["BreadcrumbList"]['itemListElement'][1:-1] #remove first and last

        for category in category_list:
            categories.append(category["name"])


        return categories



    def get_internal_id(self):
        return self.url[self.url.rfind("!")+1:-1]



    def get_content(self):
        content = ''
        
        paragraphs = self.content.xpath(".//p[contains(@class, 'bodytext')]")

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        if not content and 'articleBody' in self.json["NewsArticle"]:
            return self.json["NewsArticle"]['articleBody']

        return content


    def is_opinion(self):
        opinion = self.content.xpath(".//article/h2[contains(@class, 'seiteneinstieg-opinion')]")
        if opinion:
            return True

        return False
