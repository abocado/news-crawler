import re
from datetime import datetime

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase
from funcs import filter_authors

import json


class Crawler(CrawlerBase):
    def init(self):
        self.name = "heute"
        self.base_url = "https://www.zdf.de"
        self.landing_page = "https://www.zdf.de/nachrichten"



class LandingPage(LandingPageBase):
    def verify(self, article):
        blacklist = [
            '/in-eigener-sache/',
            #'/panorama/',
            #'/sport/',
            'feed-item',
            '/thema/'
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                return False

        return True


    def get_article_list(self):
        return self.content.xpath("//a[contains(@class, 'f1mro3s7')]")



class Article(ArticleBase):

    def parse_json(self):
        # heute uses JSON LD
        mandatory = {
            "NewsArticle": [
            ],
            "BreadcrumbList": [
                'itemListElement'
            ]
        }

        if not self._parse_json(mandatory):
            return False

        return True


    def get_date_published(self):
        return datetime.fromisoformat(self.json["NewsArticle"]['datePublished']).replace(tzinfo=None)


    def get_date_modified(self):
        return datetime.fromisoformat(self.json["NewsArticle"]['dateModified']).replace(tzinfo=None)


    def get_authors(self):
        authors = []

        if 'author' not in self.json['NewsArticle']:
            return authors

        author_list = self.json['NewsArticle']["author"]
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            if not author["name"]:
                continue
            authors.append(author["name"].strip())

        return filter_authors(authors)


    def is_paywall(self):
        return False


    def get_categories(self):
        categories = []

        length = len(self.json['BreadcrumbList']['itemListElement'])
        category_list = self.json['BreadcrumbList']['itemListElement'][1:length-1]
        
        for category in category_list:
            categories.append(category["item"]["name"])

        return categories


    def get_tags(self):
        return list(filter(len, set(self.content.xpath('.//div[@class="t130q2hl"]//span[@class="i1ozm6vy"]/text()'))))


    def get_internal_id(self):
        return self.url[self.url.rfind('/')+1:self.url.rfind('.html')]


    def get_content(self):
        content = ''
        paragraphs = self.content.xpath(".//main//div[contains(@class, 'r1nj4qn5')]")

        for text in paragraphs:
            content += text.text_content() + "\n"

        return content


    def is_opinion(self):
        return "/kommentar/" in self.url