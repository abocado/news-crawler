import re
from datetime import datetime

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Tagesspiegel"
        self.base_url = "https://www.tagesspiegel.de"
        self.landing_page = self.base_url

        self.user_agents = [
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
        ]



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//article/*[not(self::aside)]//a[@href and string-length(@href) > 0]")



    def verify(self, article):
        if not article.attrib['href'].endswith(".html"):
            return False


        blacklist = [
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                return False

        return True



class Article(ArticleBase):
    def get_tags(self):
        return self.content.xpath('.//meta[@property="article:tag"]/@content')



    def get_date_published(self):
        date = self.json["NewsArticle"]['datePublished']

        if date.endswith(".000Z"):
            date = date[:-5]
        elif date.endswith("Z"):
            date = date[:-1]

        return datetime.fromisoformat(
            date
        ).replace(tzinfo=None)



    def get_date_modified(self):
        date = self.json["NewsArticle"]['dateModified']

        if not date:
            return None

        if date.endswith(".000Z"):
            date = date[:-5]
        elif date.endswith("Z"):
            date = date[:-1]

        return datetime.fromisoformat(
            date
        ).replace(tzinfo=None)



    def get_authors(self):
        authors = []

        author_list = self.json['NewsArticle']["author"]
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            if not author["name"]:
                continue
            authors.append(author["name"].strip())


        author_list2 = self.content \
            .xpath('.//article//div[@id="story-elements"]/p[last()]/em/text()')
        if author_list2 and "(" in author_list2 and author_list2.endswith(")"):
            authors_short = author_list2[author_list2.rfind("(")+1:-1].split(',')
            for author in authors_short:
                if len(author) > 30:
                    continue

                authors.append(author)


        return list(filter(len, set(authors)))



    def is_paywall(self):
        if "isAccessibleForFree" in self.json["NewsArticle"]:
            return self.json["NewsArticle"]["isAccessibleForFree"] is not True

        return bool(self.content.xpath('.//div[@id="paywal"]')) #sic



    def get_categories(self):
        category = self.content.xpath('.//meta[@property="article:section"]/@content')

        if not category:
            return []

        return [category[0]]



    def get_internal_id(self):
        return self.url[self.url.rfind('-')+1:self.url.rfind('.html')]



    def get_content(self):
        content = ''
        paragraphs = self.content \
            .xpath('.//article//div[@id="story-elements"]/p')

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        return content



    def is_opinion(self):
        return self.content.xpath('.//meta[@property="article:opinion"]/@content')[0] != 'false'

