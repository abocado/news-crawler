import re
from datetime import datetime

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        #pylint: disable=line-too-long
        self.name = "Tichys Einblick"
        self.base_url = \
        self.landing_page = "https://www.tichyseinblick.de"
        self.user_agents = [
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
        ]



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath('//div[@id="main"]//a')



    def verify(self, article):
        blacklist = [
            '/autoren/',
            '/author/',
            '/podcast/',
            '/video/',
            '/newsletter/'
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                return False

        if article.attrib['href'].count("/") < 5:
            return False

        return True



#pylint: disable=attribute-defined-outside-init
class Article(ArticleBase):
    def parse_json(self):
        return True



    def get_permalink(self):
        link_el = self.content.xpath('.//link[@rel="shortlink"]')

        if link_el:
            link = link_el[0].attrib["href"]
            self.internal_id = link[link.rfind("=")+1:]
        else:
            link = self.url
            self.internal_id = None

        return link



    def get_tags(self):
        return []



    def get_date_published(self):
        date_published_el = self.content\
            .xpath('.//meta[@property="article:published_time"]')[0].attrib["content"].strip()

        return datetime.fromisoformat(
            date_published_el
        ).replace(tzinfo=None)



    def get_date_modified(self):
        date_modified_el = self.content \
            .xpath('.//meta[@property="article:modified_time"]')

        if not date_modified_el:
            return None

        return datetime.fromisoformat(
            date_modified_el[0].attrib["content"].strip()
        ).replace(tzinfo=None)



    def get_authors(self):
        authors = []

        author_el = self.content.xpath('//div[contains(@class, "rty-article-page-author")]//a/text()')

        if not author_el:
            return authors

        author_list = re.split(r' und |\/|, |,', author_el[0])

        for author in author_list:
            if author == 'JF-Online' or author == "Redaktion":
                continue
            authors.append(author)

        return authors



    def is_paywall(self):
        return False



    def get_categories(self):
        if '/interviews/' in self.url:
            return ["Interviews"]

        return self.content\
            .xpath('.//li[contains(@class, "current-post-ancestor")]/a/text()')



    def get_internal_id(self):
        #return self.internal_id
        link_el = self.content.xpath('.//link[@rel="shortlink"]')

        if link_el:
            link = link_el[0].attrib["href"]
            return link[link.rfind("=")+1:]
        else:
            return None



    def get_content(self):
        content = ''
        paragraphs = self.content.xpath('//div[@class="rty-article-page-content"]//p')

        if not paragraphs:
            paragraphs = self.content\
                .xpath('//div[@class="rty-article-page-content"]//div[@class="elementToProof"]')

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        return content



    def is_opinion(self):
        # far right wing magazine with strong tendency and framing in every article
        # it titles itself "the liberal-conservative opinion magazine"
        return True
