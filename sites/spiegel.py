import re
import json
import html
from datetime import datetime
import lxml.html
#import lxml.html.clean
from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase
from funcs import get_url_content, parse_content, filter_authors


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Spiegel"
        self.base_url = \
        self.landing_page = "https://www.spiegel.de"



    def get_author_detail(self):
        return #disable for now
        raw_content = get_url_content('https://www.spiegel.de/impressum')
        if not raw_content:
            return False

        parsed_content = parse_content(raw_content)

        elem = parsed_content\
            .xpath('.//div[@data-area="body"]/div/div/h3')[0]

        department = ''

        #pylint: disable=c-extension-no-member
        cleaner = lxml.html.clean.Cleaner(allow_tags=['em', "br"])

        while True:
            if elem.tag.lower() == 'h3':
                department = html.unescape(elem.text_content())
                if department == 'Verantwortlich für Anzeigen':
                    break

            elif elem.tag.lower() == 'p':
                clean = cleaner.clean_html(elem)
                string = lxml.html.tostring(clean, encoding="utf-8")\
                    .decode('utf-8').replace("<div>", "")\
                    .replace("</div>", "").replace("\n", "")

                sub_jobs = re.split("<br><em|<br>|<em",string)

                for sub_job in sub_jobs:
                    regex = re.findall(
                        r">([^:<]*):?.?<\/em>",
                        sub_job,
                        re.MULTILINE
                    )

                    if regex and len(regex) == 1:
                        job_desc = html.unescape(regex[0])
                        if job_desc == ',':
                            job_desc = ""
                    else:
                        job_desc = ""

                    regex = re.findall(
                        r"(?:,|>|^|:)([^>,:]*?) \(([^\) 0-9]{2,3})\)",
                        sub_job,
                        re.MULTILINE
                    )

                    for match in regex:
                        full_name = html.unescape(match[0].strip())
                        abbrv = match[1]

                        data = {
                            "source"    : self.name,
                            "department": department,
                            "job_desc"  : job_desc,
                            "full_name" : full_name,
                            "abbrv"     : abbrv
                        }

                        self.main.queue_save.put(['author_detail', data], True)


            elem = elem.getnext()

            if elem is None:
                break




class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//article/descendant::a[@href][1]")#[5:]



    def verify(self, article):
        swiper = article.xpath(".//ancestor::div[contains(@class, 'swiper-wrapper')]")

        if self.pos < 1 \
        and swiper is not None and len(swiper) > 0:
            return False

        return True




class Article(ArticleBase):
    def parse_json(self):
        # spiegel has a javascript json part in source wich also contains some data
        json_el = self.content.xpath('.//script[@type="application/settings+json"]/text()')
        if json_el is None or len(json_el) != 1:
            return False

        json_raw = re.search('"editorial":({.*?})\n,"paywall":', json_el[0], re.MULTILINE)
        if not json_raw:
            self.parent.log.debug("No JSON Data found! Skipping: %s", self.url)
            return False

        self.json = json.loads(json_raw.group(1))
        return True



    def get_tags(self):
        tags_el = self.content.xpath('.//meta[@name="news_keywords"]')\
            [0].attrib["content"].strip()

        # combine tags from meta and json and remove dupes
        return list(
            filter(
                len,
                set(
                    re.split(r'\/|, |,', tags_el) + self.json['info']['topics']
                )
            )
        )


    def get_title(self):
        return self.json['info']['headline']



    def get_date_published(self):
        return datetime.fromisoformat(
            self.content.xpath('.//meta[@name="date"]')[0].attrib["content"].strip()
        ).replace(tzinfo=None)



    def get_date_modified(self):
        return datetime.fromisoformat(
            self.content.xpath('.//meta[@name="last-modified"]')[0].attrib["content"].strip()
        ).replace(tzinfo=None)



    def get_authors(self):
        authors_long = self.json['author']['names']
        author_short = re.split(r'\/|, |,', self.json['author']['abbreviation'])

        # combine both author arrays and remove duplicates
        return filter_authors(authors_long + author_short)



    def is_paywall(self):
        if self.content.xpath('.//div[@data-area="paywall"]'):
            return True

        return False



    def get_categories(self):
        return re.split(r'\/|, |,', self.json['info']['channel'])



    def get_internal_id(self):
        return self.json['info']['article_id']



    def get_content(self):
        content = ''
        paragraphs = self.content.xpath('.//div[@data-sara-click-el="body_element"]//p')

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        return content



    def is_opinion(self):
        if 'category' in self.json \
        and 'type' in self.json['category']:
            if self.json['category']['type'] == 'opinion':
                return True

        return False
