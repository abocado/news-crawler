import re
from datetime import datetime

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase

#pylint: disable=attribute-defined-outside-init

class Crawler(CrawlerBase):
    def init(self):
        self.name = "Welt"
        self.base_url = \
        self.landing_page = "https://www.welt.de"

        self.user_agents = [
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
        ]



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//article//a")



    def verify(self, article):
        if not article.attrib['href'].endswith(".html"):
            return False

        blacklist = [
            '/sponsored/',
            '/services/',
            '/gallery',
            '/mediathek/'
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                return False

        return True



class Article(ArticleBase):
    def get_title(self):
        return self.json["NewsArticle"]["headline"]



    def get_description(self):
        og_desc_el = self.content.xpath('.//meta[@property="og:description"]')
        if isinstance(og_desc_el, list) and len(og_desc_el) == 1:
            og_desc = og_desc_el[0].attrib["content"].strip()
        else:
            og_desc = False

        desc_el = self.content.xpath('.//meta[@name="description"]')
        if isinstance(desc_el, list) and len(desc_el) == 1:
            desc = desc_el[0].attrib["content"].strip()
        else:
            desc = False

        first_p_el = self.content.xpath('//article//div[contains(@class, "c-article-text")]/p')
        if isinstance(first_p_el, list) and len(first_p_el) == 1:
            first_p = first_p_el[0].text_content().strip()
        else:
            first_p = False

        description = None

        if og_desc:
            description = og_desc
        elif desc:
            description = desc
        elif first_p:
            description = first_p

        return description



    def get_tags(self):
        tags = []

        keywords = self.content.xpath('.//meta[@name="keywords"]')

        if not keywords:
            return []

        tag_list = re.split(r'\/|, |,', keywords[0].attrib["content"])

        for tag in tag_list:
            tags.append(tag.strip())

        return tags



    def get_date_published(self):
        return datetime.fromisoformat(
            self.content.xpath('.//meta[@name="date"]')[0]\
                .attrib["content"].strip()[:-1]
        ).replace(tzinfo=None)



    def get_date_modified(self):
        return datetime.fromisoformat(
            self.content.xpath('.//meta[@name="last-modified"]')[0]\
                .attrib["content"].strip()[:-1]
        ).replace(tzinfo=None)



    def get_authors(self):
        authors = []

        author_list = self.json["NewsArticle"]["author"]
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            if not author["name"] or author["name"] == 'WELT':
                continue

            author_split = re.split(r' und | u. | & |\/|, |,', author["name"])

            for a in author_split:
                authors.append(a.strip())

        abbrv_authors = []
        abbrv = self.content.xpath('//div[contains(@class, "c-article-page__text")]//span[@class="c-article-page__source"]')
        if abbrv is not None and len(abbrv) == 1 and abbrv[0] is not None:
            abbrv_text = abbrv[0].text_content()
            abbrv_authors = re.split(r'\/|, |,', abbrv_text)

        return list(filter(len, set(authors + abbrv_authors)))



    def is_paywall(self):
        return self.paywall



    def get_categories(self):
        categories1 = []
        if 'articleSection' in self.json["NewsArticle"]:
            categories1 = [self.json["NewsArticle"]['articleSection']]

        categories2_el = self.content\
            .xpath('.//div[@class="c-breadcrumb"]/ol/li/a/span/text()')

        if not categories2_el or len(categories2_el) < 3:
            return categories1

        categories2 = []
        for category in categories2_el[1:-1]:
            categories2.append(category.strip())

        return list(set(categories1 + categories2))



    def get_internal_id(self):
        self.paywall = False

        match = re.findall(
            r"\/(plus|article|video|sendung)([0-9]*)\/",
            self.url
        )

        if match \
        and len(match) == 1 \
        and len(match[0]) == 2:
            if match[0][0] == 'plus':
                self.paywall = True

            return match[0][1]

        return None



    def get_content(self):
        content = ''
        paragraphs = self.content.xpath('//div[contains(@class, "c-article-page__text")]/div[contains(@class, "c-rich-text-renderer")]/p')

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        return content
