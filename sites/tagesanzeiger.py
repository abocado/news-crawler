from datetime import datetime

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase

import json


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Tagesanzeiger"
        self.base_url = "https://www.tagesanzeiger.ch"
        self.landing_page = "https://www.tagesanzeiger.ch/"



class LandingPage(LandingPageBase):
    def verify(self, article):
        blacklist = [
            'podcast',
            'kurzmeldung',
            'blog',
            'rezept',
            'sport',
            'ticker',
            'schusslers-digitale-lebenshilfe'
        ]

        for item in blacklist:
            if item in article:
                return False

        return True


    def get_article_list(self): 
        top = self.content.xpath("//a[contains(@class,'Teaser_link__aPG04')]/@href")

        json_data = self.content.xpath("//script[contains(@type, 'application/json')]")[0].text
        parsed_data = json.loads(json_data)
        bottom = []
        if parsed_data:
            for section in parsed_data['props']['pageProps']['content']['sections']:
                for teaser in section.get('teasers', []):
                    url = teaser.get('url')
                    if url:
                        bottom.append(url)
        return top + bottom
       

    def get_url(self, article):
        return article



class Article(ArticleBase):
    def parse_json(self):
        mandatory = {
            "NewsArticle": [
                'author',
                'isAccessibleForFree',
                'datePublished',
                'dateModified',
                'isAccessibleForFree',
                'genre',
                'mainEntityOfPage'
            ],
            "BreadcrumbList": [
                'itemListElement'
            ]
        }

        if not self._parse_json(mandatory):
            return False

        return True


    def get_date_published(self):
        return datetime.fromisoformat(self.json["NewsArticle"]["datePublished"]).replace(tzinfo=None)


    def get_date_modified(self):
        return datetime.fromisoformat(self.json["NewsArticle"]["dateModified"]).replace(tzinfo=None)


    def get_authors(self):
        authors = []
        for a in self.json["NewsArticle"]["author"]:
            if "name" in a:
                authors.append(a["name"].strip())
        return authors


    def is_paywall(self):
        return not self.json["NewsArticle"]["isAccessibleForFree"]


    def get_categories(self):
        categories = []
        category_list = self.json["BreadcrumbList"]['itemListElement'][1:-1] #remove first and last

        for category in category_list:
            categories.append(category["name"])


        return categories


    def get_tags(self):
        return self.json["NewsArticle"]["keywords"]


    def get_internal_id(self):
        return self.url[self.url.rfind('-')+1:]


    def get_content(self):
        content = ''
        elements = self.content.xpath('//span[contains(@class,"HtmlText_root__A1OSq")]')
        for element in elements:
            text = element.xpath('string()').strip()
            if text:
                content += text
        return content


    # def is_opinion(self):
    #   die kommentarfelder werden nicht gecrawled