import re
from datetime import datetime

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Bild"
        self.base_url = \
        self.landing_page = "https://www.bild.de"



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//div[@class='page-content']//article//a")



    def verify(self, article):
        #if not article.attrib['href'].endswith(".bild.html"):
        #    return False

        blacklist = [
            '/gewinnspiele/',
            '/ratgeber/',
            '/mein-geld/',
            '/mediathek/',
            '/kaufberater/',
            'startseite'
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                return False

        return True



class Article(ArticleBase):
    def parse_json(self):
        mandatory = {
            "NewsArticle": [
                'dateModified',
                #'author',
            ],
            "BreadcrumbList": [
                'itemListElement'
            ]
        }

        if not self._parse_json(mandatory):
            return False

        return True



    def get_tags(self):
        tags = []

        if "keywords" not in self.json["NewsArticle"] or not self.json["NewsArticle"]["keywords"]:
            return []

        tag_list = re.split(r'\/|, |,', self.json["NewsArticle"]["keywords"])

        for tag in tag_list:
            tags.append(tag.strip())

        return tags



    def get_date_published(self):
        if not self.json["NewsArticle"]["datePublished"]:
            return None

        if self.json["NewsArticle"]['datePublished'].endswith("Z"):
            published_string = self.json["NewsArticle"]['datePublished'][:-5] + "+02:00"
        else:
            published_string = self.json["NewsArticle"]['datePublished']

        return datetime.fromisoformat(
            published_string
        ).replace(tzinfo=None)



    def get_date_modified(self):
        if not self.json["NewsArticle"]["dateModified"]:
            return None

        if self.json["NewsArticle"]['dateModified'].endswith("Z"):
            modified_string = self.json["NewsArticle"]['dateModified'][:-5] + "+02:00"
        else:
            modified_string = self.json["NewsArticle"]['dateModified']

        return datetime.fromisoformat(
            modified_string
        ).replace(tzinfo=None)



    def get_authors(self):
        authors = []

        if "author" in self.json["NewsArticle"]:
            author_list = self.json["NewsArticle"]["author"]
            if isinstance(author_list, dict):
                author_list = [author_list]

            for author in author_list:
                if not author["name"] or author["name"] == 'BILD':
                    continue

                author_split = re.split(r' und | u. | & |\/|, |,', author["name"].lower())
                for a in author_split:
                    if "(" in a:
                        a = a[:a.rfind("(")]
                    authors.append(a.strip().title())


        abbrv = self.content.xpath('//article//div[@class="article-body"]/p[last()]')
        if abbrv is not None and len(abbrv) == 1 and abbrv[0] is not None:
            abbrv_text = abbrv[0].text_content()
            if "(" in abbrv_text and abbrv_text.endswith(")"):
                authors_short = abbrv_text[abbrv_text.rfind("(")+1:-1].split(',')
                for author in authors_short:
                    if len(author) > 30:
                        continue

                    authors.append(author)

        authors_filtered = []
        for author in authors:
            if author.startswith("Von "):
                author = author.replace("Von ", "")

            if author in authors_filtered:
                continue

            authors_filtered.append(author)

        return authors_filtered



    def is_paywall(self):
        if "isAccessibleForFree" in self.json["NewsArticle"]:
            return self.json["NewsArticle"]["isAccessibleForFree"] is not True

        return False



    def get_categories(self):
        categories = []
        category_list = self.json['BreadcrumbList']['itemListElement'][1:-1] #remove first and last

        for category in category_list:
            categories.append(category["item"]["name"])

        return categories



    def get_internal_id(self):
        return self.url[self.url.rfind('-')+1:self.url.rfind('.bild.html')]



    def get_content(self):
        content = ''
        paragraphs = self.content.xpath('//article//div[@class="article-body"]/p')

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        return content



    def is_opinion(self):
        return True
