from datetime import datetime
import re
import json
from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase
from funcs import filter_authors


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Focus"
        self.base_url = \
        self.landing_page = "https://www.focus.de/"



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//a[@data-vr-contentbox]")



    def verify(self, article):
        top = article.xpath('.//ancestor::div[@id="topArticle"]')

        if not article.attrib['href'].endswith(".html"):
            return False

        blacklist = [
            '/video/',
            '/deals/',
            '/weinstube/',
            '/config/'
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                if top:
                    self.pos += 1

                return False

        if self.pos < 1 \
        and (top is None or len(top) == 0):
            return False

        return True



class Article(ArticleBase):
    def parse_json(self):
        if not self._parse_json():
            return False

        json_el = self.content.xpath('.//script[@type="text/javascript"]/text()')
        for json_data in json_el:
            json_raw = re.search(
                r'window\.bf__bfa_metadata = {}; try {window\.bf__bfa_metadata = (.*);} catch\(oError\) {};',
                json_data,
                re.MULTILINE
            )

            if not json_raw:
                continue

            self.json_extra = json.loads(json_raw.group(1))

        return True



    def get_description(self):
        desc_el = self.content.xpath('.//meta[@property="og:description"]')

        if not desc_el:
            if self.json and 'description' in self.json:
                return self.json['description'].strip()
            else:
                return ''

        return desc_el[0].attrib["content"].strip()



    def get_tags(self):
        if not self.json_extra or "keywords" not in self.json_extra:
            return []

        tags = []

        for keyword in self.json_extra["keywords"]:
            tags.append(keyword["name"])

        return tags



    def get_date_published(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]["datePublished"]
        ).replace(tzinfo=None)



    def get_date_modified(self):
        return datetime.fromisoformat(
            self.json['NewsArticle']["dateModified"]
        ).replace(tzinfo=None)



    def get_authors(self):
        authors_long = self.content\
            .xpath('.//span/a[@rel="author"]/text()')


        authors_short = []
        abbrv = self.content.xpath('//span[@class="created"]/text()')
        if abbrv is not None and len(abbrv) == 1 and abbrv[0]:
            authors_short = re.split(r'\/|, |,', abbrv[0])

        # combine both author arrays and remove duplicates
        return filter_authors(authors_long + authors_short)



    def is_paywall(self):
        return False



    def get_categories(self):
        categories = []

        category_list = self.json['BreadcrumbList']['itemListElement'][1:]

        for category in category_list:
            categories.append(category["item"]["name"])

        return categories



    def get_content(self):
        content = ''
        paragraphs = self.content\
            .xpath('.//article//div[contains(@class, "textBlock")]/p ' \
                 + '| .//article/section[contains(@class, "Article-Content")]/div/p' \
                 + '| .//article/div[contains(@class, "Article-Content")]/div/p' \
            )

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        return content



    def get_internal_id(self):
        internal_id = self.content.xpath('.//meta[@name="fol:articleid"]')

        if internal_id:
            return internal_id[0].attrib["content"].strip()

        return self.url[self.url.rfind('_')+1:self.url.rfind('.html')]