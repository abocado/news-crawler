from datetime import datetime
import re
from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "n-tv"
        self.base_url = \
        self.landing_page = "https://www.n-tv.de"



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//article/div/a")



    def verify(self, article):
        if not article.attrib['href'].endswith(".html"):
            return False

        if '/mediathek/' in article.attrib['href']:
            return False

        return True



class Article(ArticleBase):
    def parse_json(self):
        mandatory = {
            "NewsArticle": [
                'author'
            ],
            "BreadcrumbList": [
                'itemListElement'
            ]
        }

        if not self._parse_json(mandatory):
            return False

        return True



    def get_image_url(self):
        image = self.content.xpath('.//meta[@property="og:image"]')

        if image is not None and len(image) == 1:
            return image[0].attrib["content"].strip()

        return None



    def get_tags(self):
        tags = []

        keywords = self.content.xpath('.//meta[@name="keywords"]')

        if not keywords:
            return []

        tag_list = re.split(r'\/|, |,', keywords[0].attrib["content"])

        for tag in tag_list:
            tags.append(tag.strip())

        return tags



    def get_date_published(self):
        date_published_el = self.content \
            .xpath('.//meta[@name="date"]')[0].attrib["content"].strip()

        return datetime.fromisoformat(
            date_published_el
        ).replace(tzinfo=None)



    def get_date_modified(self):
        date_published_el = self.content \
            .xpath('.//meta[@name="last-modified"]')[0].attrib["content"].strip()

        return datetime.fromisoformat(
            date_published_el
        ).replace(tzinfo=None)



    def get_authors(self):
        authors = []

        author_list = self.json['NewsArticle']["author"]
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            if not author["name"] or 'n-tv NACHRICHTEN' in author["name"]:
                continue

            authors.append(author["name"].strip())

        abbrv = self.content\
            .xpath('.//p[@class="article__source"]/text()')

        if abbrv is not None and len(abbrv) == 1:
            authors_short = re.split(r' und |&|\/|,', abbrv[0])

            for author in authors_short:
                if "ntv.de" in author or author in authors or len(author.split()) > 3:
                    continue

                authors.append(author.strip())

        author_alternative_el = self.content.xpath('.//span[contains(@class, "article__author")]/text()')
        if author_alternative_el:
            author_alternative = re.split(r'und|\/|&|,', author_alternative_el[0].replace("von", "").replace("Von", ""))
            for author in author_alternative:
                authors.append(author.strip())

        return list(filter(len, set(authors)))



    def is_paywall(self):
        return False



    def is_opinion(self):
        return '/kommentare/' in self.url



    def get_categories(self):
        categories = []

        category_list = self.json['BreadcrumbList']['itemListElement'][1:-1]

        for category in category_list:
            categories.append(category["item"]["name"])

        return categories



    def get_content(self):
        content = ''
        paragraphs = self.content\
            .xpath('.//div[@class="article__text"]/p[not(contains(@class, "article__source"))]')

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        return content




    def get_internal_id(self):
        #return self.url[self.url.rfind('-article')+1:self.url.rfind('.html')]
        return self.content.xpath('.//meta[@property="article:hash"]')[0]\
            .attrib["content"].strip()
