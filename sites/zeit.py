import re
from datetime import datetime

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Zeit"
        self.base_url = "https://www.zeit.de"
        self.landing_page = "https://www.zeit.de/index"



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//article/a")

    def verify(self, article):
        blacklist = [
            '/video/',
            '/zett/',
            '/newsletter/',
            '/wochenende/'
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                return False

        return True



class Article(ArticleBase):
    def get_tags(self):
        tags = []

        tag_list_el = self.content \
            .xpath('.//meta[@name="keywords"]')

        if not tag_list_el:
            return tags

        tag_list = tag_list_el[0].attrib["content"].split(",")

        for tag in tag_list:
            tags.append(tag.strip())

        return tags



    def get_date_published(self):
        date_published_el = self.content \
            .xpath('.//meta[@name="date"]')[0].attrib["content"].strip()

        return datetime.fromisoformat(
            date_published_el
        ).replace(tzinfo=None)



    def get_date_modified(self):
        date_published_el = self.content \
            .xpath('.//meta[@name="last-modified"]')[0].attrib["content"].strip()

        return datetime.fromisoformat(
            date_published_el
        ).replace(tzinfo=None)



    def get_authors(self):
        authors = []

        author_list = self.json['NewsArticle']["author"]
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            if not author["name"]:
                continue
            authors.append(author["name"].strip())


        author_list2 = self.content \
            .xpath('.//a[@rel="author"]/span[@itemprop="name"]/text()')
        for author in author_list2:
            authors.append(author)

        return list(filter(len, set(authors)))



    def is_paywall(self):
        if "isAccessibleForFree" in self.json["NewsArticle"]:
            return self.json["NewsArticle"]["isAccessibleForFree"] != "True"

        return bool(self.content.xpath('.//div[contains(@class, "zplus-badge")]'))



    def get_categories(self):
        site = self.content\
            .xpath('.//meta[@property="og:site_name"]')[0].attrib["content"].strip()

        if site == "ZEIT ONLINE":
            category_el = self.content\
                .xpath('.//a[@class="nav__ressorts-link--current"]/span[@itemprop="name"]/text()')

            if category_el:
                category = category_el[0].strip()
            else:
                category = "Agentur"

        else:
            category = site

        return [category]



    def get_internal_id(self):
        internal_id = self.content.xpath('.//body')[0].attrib["data-uuid"]

        return internal_id



    def get_content(self):
        content = ''
        paragraphs = self.content.xpath('.//p[@class="paragraph article__item"]')

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        return content



    def is_opinion(self):
        opinion = self.content\
            .xpath('.//div[@data-ct-row="author"]/text()')

        if isinstance(opinion, list) \
        and len(opinion) > 0 \
        and (
            'Kommentar' in opinion[0] \
            or "Kolumne" in opinion[0]
        ):
            return True

        return False
