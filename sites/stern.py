from datetime import datetime
import re

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Stern"
        self.base_url = \
        self.landing_page = "https://www.stern.de"



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//article//a")



    def verify(self, article):
        if not article.attrib['href'].endswith(".html"):
            return False

        blacklist = [
            'podcast'
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                return False

        return True



class Article(ArticleBase):
    def parse_json(self):
        mandatory = {
            "NewsArticle": [
                'dateModified',
                'author',
            ],
            "BreadcrumbList": [
                'itemListElement'
            ]
        }

        if not self._parse_json(mandatory):
            return False

        return True



    def get_title(self):
        title_el = self.content.xpath('.//meta[@property="og:title"]')
        if not title_el:
            title_el = self.content.xpath('.//meta[@name="sis-article-headline"]')

        if not title_el:
            return None

        return title_el[0].attrib["content"].strip()



    def get_description(self):
        desc_el = self.content.xpath('.//meta[@property="og:description"]')
        if not desc_el:
            if 'description' in self.json["NewsArticle"]:
                return self.json["NewsArticle"]["description"]

            return ""

        return desc_el[0].attrib["content"].strip()



    def get_tags(self):
        return self.content.xpath('.//meta[@property="article:tag"]/@content')




    def get_date_published(self):
        return datetime.fromisoformat(
            self.json['NewsArticle']["datePublished"]
        ).replace(tzinfo=None)



    def get_date_modified(self):
        return datetime.fromisoformat(
            self.json['NewsArticle']["dateModified"]
        ).replace(tzinfo=None)



    def get_authors(self):
        authors = []
        author_list = self.json['NewsArticle']["author"]
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            if not author["name"] or author["name"] == "STERN.de":
                continue
            authors.append(author["name"])


        author_short_el = self.content.xpath('.//span[@class="credits-author-source__item"]/text()')
        if author_short_el:
            author_shorts = re.split(r'und|\/|, |,', author_short_el[0])
            # remove dupes from authors
            authors = list(filter(len, set(authors + author_shorts)))

        author_alternative_el = self.content.xpath('.//div[contains(@class, "authors__text")]/text()')
        if author_alternative_el:
            author_alternative = re.split(r'und|\/|, |,', author_alternative_el[0].replace("von", "").replace("Von", ""))
            for author in author_alternative:
                authors.append(author.strip())

            authors = list(filter(len, set(authors)))

        return authors



    def is_paywall(self):
        if "isAccessibleForFree" not in self.json['NewsArticle']:
            return False

        return self.json['NewsArticle']["isAccessibleForFree"].lower() == "false"



    def get_categories(self):
        categories = []

        category_list = self.json['BreadcrumbList']['itemListElement'][1:-1]

        for category in category_list:
            categories.append(category["item"]["name"])

        return categories



    def get_content(self):
        content = ''
        paragraphs = self.content\
            .xpath('.//article//div[contains(@class, "article__body")]//p')

        if not paragraphs:
            paragraphs = self.content.xpath(
                './/article//*[contains(@class, "u-richtext") and contains(@class, "u-typo")]'
            )

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        return content



    def get_internal_id(self):
        return self.url[self.url.rfind('-')+1:self.url.rfind('.html')]
