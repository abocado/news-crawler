import re
from datetime import datetime

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Tagesschau"
        self.base_url = \
        self.landing_page = "https://www.tagesschau.de"



class LandingPage(LandingPageBase):
    def verify(self, article):
        blacklist = [
            '/multimedia/',
            '/spendenkonten/'
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                return False

        return True



    def get_article_list(self):
        return self.content.xpath("//a[contains(@class, 'teaser__link')]")



class Article(ArticleBase):
    def get_tags(self):
        return self.content.xpath('.//li[@class="taglist__element"]/a/text()')



    def get_date_published(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]['datePublished']
        ).replace(tzinfo=None)



    def get_date_modified(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]['dateModified']
        ).replace(tzinfo=None)



    def get_authors(self):
        authors_string = self.content.xpath(".//div[contains(@class, 'authorline__author')]/text()")
        if not authors_string:
            return []

        regex = re.findall(
            r"(?:[Vv]on |Ein Kommentar von |Das Gespräch führte "+\
            r"|Mit Informationen von |Das Interview führte |Mitarbeit: "+\
            r"|führte |, |und |^|zurzeit |zzt\. )+((?:(?:[a-zA-Z\-äöüÄÖÜß\/]|\."+\
            r" )+ ?){2,4})(?=und|,|\.\n|\.$| $|$|für|\u00a0|\()", authors_string[0]
        )

        authors = []
        for author in regex:
            authors.append(author.strip())

        return authors



    def is_paywall(self):
        return False



    def get_categories(self):
        categories = []

        category_list = self.json['BreadcrumbList']['itemListElement'][1:]

        for category in category_list:
            categories.append(category["item"]["name"])

        if not categories and '/newsticker/' in self.url:
            categories = ["Newsticker"]

        return categories



    def get_internal_id(self):
        return self.url[self.url.rfind('/')+1:self.url.rfind('.html')]



    def get_content(self):
        content = ''
        paragraphs = self.content.xpath(".//p[contains(@class, 'textabsatz')]")

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        return content



    def is_opinion(self):
        return "/kommentar/" in self.url
