import re
from datetime import datetime

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Junge Freiheit"
        self.base_url = \
        self.landing_page = "https://jungefreiheit.de"



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//article[contains(@class, 'post')]/a")



    def verify(self, article):
        return True



#pylint: disable=attribute-defined-outside-init
class Article(ArticleBase):
    def parse_json(self):
        mandatory = {
            "NewsArticle": [
                'dateModified'
            ],
            "Person": [
                'name'
            ]
        }

        if not self._parse_json(mandatory):
            return False

        return True



    def get_permalink(self):
        link = self.content.xpath('.//link[@rel="shortlink"]')[0]\
            .attrib["href"]

        self.internal_id = link[link.rfind("=")+1:]

        return link



    def get_tags(self):
        return self.content.xpath('.//meta[@property="article:tag"]/@content')



    def get_date_published(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]['datePublished']
        ).replace(tzinfo=None)



    def get_date_modified(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]['dateModified']
        ).replace(tzinfo=None)



    def get_authors(self):
        authors_long = []

        author_list = re.split(r' und |\/|, |,', self.json["Person"]["name"])

        for author in author_list:
            if author == 'JF-Online':
                continue
            authors_long.append(author)

        authors_short = []
        last_p = self.content.xpath('(//div[@data-widget_type="theme-post-content.default"]//p)[last()]')
        if last_p is not None and len(last_p) == 1 and last_p[0] is not None:
            last_p = last_p[0].text_content().replace("\n","").strip()
            
            if last_p.endswith(")"):
                abbrv = last_p[last_p.rfind("(")+1:-1]
                authors_short = re.split(r'\/|, |,', abbrv)

        # combine both author arrays and remove duplicates
        return list(filter(len, set(authors_long + authors_short)))



    def is_paywall(self):
        return self.content.xpath('//img[@alt="JF-Plus Icon Premium"]') != []



    def get_categories(self):
        return [self.content.xpath('.//meta[@property="article:section"]')[0]\
            .attrib["content"]]



    def get_internal_id(self):
        link_el = self.content.xpath('.//link[@rel="shortlink"]')

        if link_el:
            link = link_el[0].attrib["href"]
            return link[link.rfind("=")+1:]
        else:
            return None



    def get_content(self):
        content = ''
        paragraphs = self.content.xpath('//div[@data-widget_type="theme-post-content.default"]//p')

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", "") + "\n"

        return content



    def is_opinion(self):
        # far right wing magazine with strong tendency and framing in every article
        return True
