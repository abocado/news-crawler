import re
import html
import lxml.html
from datetime import datetime
from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Neues Deutschland"
        self.base_url = \
        self.landing_page = "https://www.nd-aktuell.de"



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath('//main//article//div[@class="Title"]/a')



class Article(ArticleBase):
    def get_title(self):
        return self.json["NewsArticle"]["headline"]



    def get_tags(self):
        tags = []
        tag_list = self.json["NewsArticle"]['keywords'].split(",")

        for tag in tag_list:
            tag = tag.strip()
            tags.append(tag)

        return tags



    def get_date_published(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]['datePublished'][:-2] + ":00"
        ).replace(tzinfo=None)



    def get_date_modified(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]['dateModified'][:-2] + ":00"
        ).replace(tzinfo=None)



    def get_authors(self):
        authors = []

        if 'author' not in self.json["NewsArticle"]:
            return authors

        author_list = self.json["NewsArticle"]["author"]
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            author_split = re.split(r' und | u. | & |\/|, |,', author["name"])

            for a in author_split:
                authors.append(a.strip())

        return authors



    def is_paywall(self):
        return False



    def get_categories(self):
        return [self.json["NewsArticle"]["genre"]]



    def get_internal_id(self):
        url = self.url.replace(self.parent.base_url, "")

        return url[9:url.find('.')]



    def get_content(self):
        html_fragment = html.unescape(self.json["NewsArticle"]["articleBody"])
        content = lxml.html.fromstring(html_fragment).text_content()
        return content
