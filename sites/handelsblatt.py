from datetime import datetime
import json
from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Handelsblatt"
        self.base_url = "https://www.handelsblatt.com"
        self.landing_page = "https://www.handelsblatt.com/contentexport/feed/top-themen"
        self.content_url = "https://content.www.handelsblatt.com"
        self.xml = True
        self.json_redirect = True

        if self.name in self.main.known_urls:
            for i, url in enumerate(self.main.known_urls[self.name]):
                self.main.known_urls[self.name][i] = \
                    url.replace(
                        self.base_url,
                        self.content_url + '/api/content/eager/?url='
                    )





class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//link/text()")



    def get_url(self, article):
        return article.replace(self.base_url, self.parent.content_url + '/api/content/eager/?url=')



    def verify(self, article):
        return True


    def save_position(self, pos, url):
        url = url.replace("/api/content/eager/?url=", "").replace(self.parent.content_url, self.base_url)

        data = {
            "url"       : url,
            "position"  : pos
        }

        self.main.queue_save.put(['position', data], True)



class Article(ArticleBase):
    def parse_json(self):
        try:
            self.content = json.loads(self.content_raw)

            json_ld = json.loads(self.content['seo']['jsonLd'])

            self.json = {
                "BreadcrumbList": json_ld[0],
                "NewsArticle"   : json_ld[1]
            }

        except Exception as err:
            print("JSON COULD NOT BE PARSED", err)
            print(f"JSON STRING >>>{self.content_raw[:200]}<<<")
            print(self.url)
            return False

        return True



    def get_internal_id(self):
        # hack to save link for humans instead of json
        self.url = self.url.replace("/api/content/eager/?url=", "").replace(self.parent.parent.content_url, self.parent.base_url)

        if not self.url.endswith(".html"):
            return None

        return self.url[self.url.rfind('/')+1:-5]




    def get_title(self):
        return self.content['header']['headline'].strip()



    def get_description(self):
        return self.content['header']['leadText'].strip()



    def get_image_url(self):
        if 'image' not in self.json["NewsArticle"] or not self.json["NewsArticle"]['image']:
            return None

        image = self.json["NewsArticle"]['image'][0]

        if isinstance(image, dict) and 'url' in image:
            image = image['url']

        return image



    def get_date_published(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]["datePublished"]
        ).replace(tzinfo=None)



    def get_date_modified(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]["dateModified"]
        ).replace(tzinfo=None)



    def get_categories(self):
        return [self.json["NewsArticle"]['articleSection']]



    def get_authors(self):
        authors = []

        author_list = self.json["NewsArticle"]["author"]
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            authors.append(author["name"])

        return authors



    def get_tags(self):
        return self.json["NewsArticle"]["keywords"]



    def get_content(self):
        if 'articleBody' not in self.json["NewsArticle"]:
            return False

        return self.json["NewsArticle"]['articleBody']



    def is_paywall(self):
        return not self.json["NewsArticle"]["isAccessibleForFree"].lower() == "true"



    def is_opinion(self):
        return self.content["meta"]["type"] == "comment"
