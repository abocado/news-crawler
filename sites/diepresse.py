import re
from datetime import datetime

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase

import json


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Die Presse"
        self.base_url = "https://www.diepresse.com/"
        self.landing_page = "https://www.diepresse.com/"



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//a[contains(@class,'card__link')]")
        


class Article(ArticleBase):
    def parse_json(self):
        mandatory = {
            "NewsArticle": [
            ]
        }

        if not self._parse_json(mandatory):
            return False

        return True


    def get_date_published(self):
        date_str = self.json["NewsArticle"]['datePublished']
        return datetime.fromisoformat(date_str).replace(tzinfo=None)
    

    def get_date_modified(self):
        date_str = self.content.xpath("//meta[contains(@name,'article:modified_time')]/@content")
        return datetime.fromisoformat(date_str[0]).replace(tzinfo=None)


    def get_authors(self):
        authors = []

        if 'author' not in self.json['NewsArticle']:
            return authors

        author_list = self.json['NewsArticle']['author']
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            if not 'name' in author:
                authors.append(author.strip())
            else:
                authors.append(author["name"].strip())

        return authors


    def is_paywall(self):
        if 'isAccessibleForFree' in self.json['NewsArticle']:
            if self.json['NewsArticle']['isAccessibleForFree'] == False :
                return True
        
        return False


    def get_categories(self):
        category = self.content.xpath("//meta[contains(@name,'cXenseParse:recs:category')]/@content")
        return category


    def get_tags(self):
        # there are tags for Google, but typically empty
        scripts = self.content.xpath('//script')
 
        for script in scripts:
            script_content = script.text_content()
            if 'var dataLayer = window.dataLayer || [];' in script_content:
                match = re.search(r"'pageTags':\s*'([^']*)'", script_content)
                if match:
                    return match.group(1)

        return []


    def get_internal_id(self):
        url = self.url.replace(self.parent.base_url, "")
        return url[:url.find("/")]


    def get_content(self):
        texts = self.content.xpath('//div[@id="article-body"]//p | //div[@id="article-body"]//h2')

        content= ""
        for text in texts:
            content += text.text_content().strip()

        return content


    def is_opinion(self):
        opinion = self.content.xpath("//meta[@name='keywords']/@content")

        if opinion and 'ommentar' in opinion[0]:
            return True

        return False
