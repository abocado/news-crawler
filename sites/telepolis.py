import re
from datetime import datetime
from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase
from funcs import filter_authors


class Crawler(CrawlerBase):
    def init(self):
        self.name = "Telepolis"
        self.base_url = \
        self.landing_page = "https://www.telepolis.de"




class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//main//a")



    def verify(self, article):
        if '/seite-2/' == article.attrib['href']:
            return False

        if not article.attrib['href'].endswith(".html"):
            return False

        return True



    def get_url(self, article):
        return article.attrib['href'] + "?seite=all"




class Article(ArticleBase):
    def get_tags(self):
        tags = []

        tag_list = re.split(r'\/|, |,', self.json["NewsArticle"]["keywords"])

        for tag in tag_list:
            tags.append(tag.strip())

        return tags



    def get_date_published(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]['datePublished']
        ).replace(tzinfo=None)



    def get_date_modified(self):
        return datetime.fromisoformat(
            self.json["NewsArticle"]['dateModified']
        ).replace(tzinfo=None)



    def get_authors(self):
        authors = []

        author_list = self.json["NewsArticle"]["author"]
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            if not author['name']:
                continue

            authors.append(author["name"])

        authors = filter_authors(authors)

        return authors



    def is_paywall(self):
        return False
        #pylint: disable=unreachable, pointless-string-statement
        '''
        if isinstance(self.json["NewsArticle"]["isAccessibleForFree"], str):
            return not self.json["NewsArticle"]["isAccessibleForFree"].lower() == "true"

        return not self.json["NewsArticle"]["isAccessibleForFree"]
        '''



    def get_categories(self):
        categories = []

        for category in self.content.xpath('.//ul[contains(@class, "navbar-nav")]/li[@class="active"]/a/text()'):
            categories.append(category.replace("\n","").strip())

        return categories




    def get_internal_id(self):
        return self.url[self.url.rfind('-')+1:self.url.rfind('.html')]



    def get_content(self):
        content = ''
        paragraphs = self.content.xpath(".//main/article/p")

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip().replace("\n", "").replace("\r", "").replace("\xa0", " ") + "\n"

        return content



    def is_opinion(self):
        return True
