from datetime import datetime
import re
import json
from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase


class Crawler(CrawlerBase):
    def init(self):
        self.name = "rnd"
        self.base_url = \
        self.landing_page = "https://www.rnd.de"



class LandingPage(LandingPageBase):
    def get_article_list(self):
        return self.content.xpath("//a[div/article]")



    def verify(self, article):
        if not article.attrib['href'].endswith(".html"):
            return False

        return True



class Article(ArticleBase):
    def parse_json(self):
        if not self._parse_json():
            return False

        json_el = self.content.xpath('.//script[@id="fusion-metadata"]/text()')

        if json_el is None or len(json_el) != 1:
            self.parent.log.debug("No JSON Extra found! Skipping: %s", self.url)
            return False

        json_raw = re.search(
            r';Fusion.globalContent=(.*);Fusion.globalContentConfig=',
            json_el[0],
            re.MULTILINE
        )

        if not json_raw:
            self.parent.log.debug("No JSON Extra found! Skipping: %s", self.url)
            return False

        self.json_extra = json.loads(json_raw.group(1))

        return True



    def get_tags(self):
        return self.content.xpath('.//li/a[@data-vars-event-category="tagwolke"]/text()')



    def get_date_published(self):
        return datetime.fromisoformat(
            self.json['NewsArticle']["datePublished"]
        ).replace(tzinfo=None)



    def get_date_modified(self):
        return datetime.fromisoformat(
            self.json['NewsArticle']["dateModified"]
        ).replace(tzinfo=None)



    def get_authors(self):
        authors = []

        author_list = self.json['NewsArticle']["author"]
        if isinstance(author_list, dict):
            author_list = [author_list]

        for author in author_list:
            if not author["name"] or 'RedaktionsNetzwerk Deutschland' in author["name"]:
                continue

            author = " ".join(author["name"].split())

            authors.append(author.strip())


        abbrv = self.content\
            .xpath('.//article/div//p[last()]//i/text()')

        if abbrv is not None and len(abbrv) == 1:
            authors_short = abbrv[0].split("/")

            for author in authors_short:
                if len(author) > 30 and len(author.split()) > 2:
                    continue

                if "RND" in author or author in authors:
                    continue

                author = " ".join(author.split())

                authors.append(author.strip())


        return authors



    def is_paywall(self):
        if "isAccessibleForFree" not in self.json['NewsArticle']:
            return False

        if isinstance(self.json['NewsArticle']["isAccessibleForFree"], str):
            return self.json['NewsArticle']["isAccessibleForFree"].lower() == "false"
        elif isinstance(self.json['NewsArticle']["isAccessibleForFree"], bool):
            return self.json['NewsArticle']["isAccessibleForFree"] is False



    def is_opinion(self):
        return self.json_extra['isOpinionArticle']



    def get_categories(self):
        categories = []

        category_list = self.json['BreadcrumbList']['itemListElement'][:-1]

        for category in category_list:
            categories.append(category["item"]["name"])

        return categories



    def get_content(self):
        content = ''
        paragraphs = self.content\
            .xpath('.//article[@data-element-selector="article"]/div//p')

        for text in paragraphs:
            content += re.sub(
                r'(\n\s*)',
                '',
                text.text_content()
            ).strip()\
            .replace("\n", "")\
            .replace("\r", "")\
            .replace("\xa0", "") + "\n"

        return content



    def get_internal_id(self):
        return self.url[self.url.rfind('-')+1:self.url.rfind('.html')]
