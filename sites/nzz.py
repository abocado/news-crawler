import re
from datetime import datetime

from crawler import CrawlerBase
from landing_page import LandingPageBase
from article import ArticleBase

import json


class Crawler(CrawlerBase):
    def init(self):
        self.name = "NZZ"
        self.base_url = "https://www.nzz.ch"
        self.landing_page = "https://www.nzz.ch/"



class LandingPage(LandingPageBase):
    def verify(self, article):
        blacklist = [
            '/feuilleton/',
            '/visuals/',
            '/meinung/',
            '/reisen/'
        ]

        for item in blacklist:
            if item in article.attrib['href']:
                return False

        return True


    def get_article_list(self):
        return self.content.xpath("//div[contains(@class, 'teaser__content')]/a")



class Article(ArticleBase):

    def parse_json(self):
        mandatory = {
            "NewsArticle": [
                "datePublished",
                "dateModified",
            ],
            "BreadcrumbList": [
                "itemListElement"
            ]
        }
        if not self._parse_json(mandatory):
            return False

        return True


    def get_date_published(self):
        return datetime.fromisoformat(self.json["NewsArticle"]["datePublished"]).replace(tzinfo=None)


    def get_date_modified(self):
        return datetime.fromisoformat(self.json["NewsArticle"]["dateModified"]).replace(tzinfo=None)


    def get_authors(self):
        authors = []

        author_list = self.json["NewsArticle"]["author"]
        
        if not author_list:
            return authors

        for a in author_list: 
            if not a["name"]:
                continue
            authors.append(a["name"].strip())

        return list(filter(len, set(authors)))


    def is_paywall(self):
        return not self.json["NewsArticle"]["isAccessibleForFree"]


    def get_categories(self):
        categories = []

        length = len(self.json['BreadcrumbList']['itemListElement'])
        category_list = self.json['BreadcrumbList']['itemListElement'][1:length-1]
        
        for category in category_list:
            categories.append(category["name"])

        return categories


    def get_tags(self):
        # no tags ?!
        return []


    def get_internal_id(self):
        return self.url[self.url.rfind('.')+1:]


    def get_content(self):
        content = ''
        for c in self.content.xpath("//p[contains(@class, 'articlecomponent text')]/text()"):
            content += c
        return content


    def is_opinion(self):
        return "/kommentar/" in self.url
