#!/bin/bash
echo news-crawler database structure dump

read -p "Host [localhost]: " host
host=${host:-localhost}

read -p "User [root]: " user
user=${user:-root}

read -p "Password: " -s password
echo

read -p "Database name [news]: " database
database=${database:-news}

echo "Dumping database \"$database\""
mariadb-dump --skip-comments --events --routines --no-data -h localhost -u$user -p$password $database | sed 's/ AUTO_INCREMENT=[0-9]*//g' | sed -e 's/DEFINER=[^ |\*]*//' | sed -e "s/STARTS [^ON|\*]*//" > news.sql
mariadb-dump --skip-comments --no-create-info -h localhost -u$user -p$password $database sources threshold_links >> news.sql

echo "Finished"