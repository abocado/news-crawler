#!/bin/bash
echo news-crawler database installation

read -p "Host [localhost]: " host
host=${host:-localhost}

read -p "User [root]: " user
user=${user:-root}

read -p "Password: " -s password
echo

read -p "Database name [news]: " database
database=${database:-news}


mariadb -h $host -u$user -p$password -e "CREATE DATABASE IF NOT EXISTS $database CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
mariadb -h $host -u$user -p$password $database < news.sql

echo "Finished"
echo "Don't forget to setup config.ini!"