
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` int(11) NOT NULL,
  `uaid` varchar(100) DEFAULT NULL,
  `url` text NOT NULL,
  `image_url` text DEFAULT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `content` mediumtext /*!100301 COMPRESSED*/ DEFAULT NULL,
  `paywall` tinyint(1) DEFAULT NULL,
  `opinion` tinyint(1) DEFAULT NULL,
  `date_published` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_downloaded` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `modified` smallint(6) NOT NULL DEFAULT 0,
  `hours` mediumint(9) DEFAULT NULL,
  `average_position` smallint(6) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `highest_position` smallint(6) DEFAULT NULL,
  `lowest_position` smallint(6) DEFAULT NULL,
  `first_seen` datetime DEFAULT current_timestamp(),
  `last_seen` datetime DEFAULT NULL,
  `url_sha256` binary(64) GENERATED ALWAYS AS (sha2(`url`,256)) VIRTUAL,
  `image_url_sha256` binary(64) GENERATED ALWAYS AS (sha2(`image_url`,256)) VIRTUAL,
  `title_sha256` binary(64) GENERATED ALWAYS AS (sha2(`title`,256)) VIRTUAL,
  `description_sha256` binary(64) GENERATED ALWAYS AS (sha2(`description`,256)) VIRTUAL,
  `content_sha256` binary(64) GENERATED ALWAYS AS (sha2(`content`,256)) VIRTUAL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_date` (`id`,`date_downloaded`),
  UNIQUE KEY `full` (`id`,`source`,`date_downloaded`),
  UNIQUE KEY `unique` (`source`,`uaid`),
  KEY `image_url_sha256` (`image_url_sha256`),
  KEY `title_sha256` (`title_sha256`),
  KEY `description_sha256` (`description_sha256`),
  KEY `content_sha256` (`content_sha256`),
  KEY `date_published` (`date_published`),
  KEY `date_modified` (`date_modified`),
  KEY `date_downloaded` (`date_downloaded`),
  KEY `paywall` (`paywall`),
  KEY `opinion` (`opinion`),
  KEY `url_sha256` (`url_sha256`) USING BTREE,
  KEY `uaid` (`uaid`),
  KEY `source` (`source`),
  KEY `source_date` (`source`,`date_published`,`date_modified`) USING BTREE,
  KEY `first_seen` (`first_seen`) USING BTREE,
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `title` (`title`),
  FULLTEXT KEY `title_description_full` (`title`,`description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `author_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abbrv` varchar(10) NOT NULL,
  `full_name` varchar(200) NOT NULL,
  `source` int(11) NOT NULL,
  `department` varchar(200) NOT NULL,
  `job_description` varchar(200) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `abbrv` (`abbrv`,`full_name`,`source`,`department`,`job_description`),
  KEY `source_link` (`source`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `author_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author_links` (
  `article_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  UNIQUE KEY `article_id` (`article_id`,`author_id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  FULLTEXT KEY `name_2` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  FULLTEXT KEY `name_2` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `category_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_links` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  UNIQUE KEY `article_id` (`article_id`,`category_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `text` mediumtext /*!100301 COMPRESSED*/ NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `crawl_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crawl_results` (
  `source` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `articles_found` int(11) NOT NULL,
  `articles_saved` int(11) NOT NULL,
  UNIQUE KEY `source` (`source`,`date`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `descriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `descriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `text` text /*!100301 COMPRESSED*/ NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `days` int(11) NOT NULL,
  `group_nr` int(11) NOT NULL,
  `rank` int(11) DEFAULT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`days`,`group_nr`,`article_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `timestamp` double NOT NULL,
  `level` int(11) NOT NULL,
  `thread` varchar(100) NOT NULL,
  `module` varchar(100) NOT NULL,
  `func` varchar(100) NOT NULL,
  `line` int(11) NOT NULL,
  `msg` text NOT NULL,
  PRIMARY KEY (`timestamp`,`level`,`thread`,`module`,`func`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `position_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `position_cache` (
  `date` datetime NOT NULL,
  `url` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `analyzed` tinyint(4) NOT NULL DEFAULT 0,
  UNIQUE KEY `unique` (`date`,`url`),
  KEY `url` (`url`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions` (
  `date` datetime NOT NULL,
  `url` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  UNIQUE KEY `position_unique` (`url`,`date`) USING BTREE,
  KEY `date` (`date`),
  KEY `url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `min_hourly_position_count` int(11) NOT NULL DEFAULT 10,
  `min_hourly_article_count` int(11) NOT NULL DEFAULT 0,
  `min_hourly_crawl_result_count` int(11) NOT NULL DEFAULT 1,
  `min_article_count` int(11) NOT NULL DEFAULT 10,
  `min_article_modification_count` int(11) NOT NULL DEFAULT 0,
  `max_property_modification_count` int(11) NOT NULL DEFAULT 15,
  `max_average_modification_count` int(11) NOT NULL DEFAULT 2,
  `min_percent_authors` int(11) NOT NULL DEFAULT 100,
  `min_percent_categories` int(11) NOT NULL DEFAULT 100,
  `min_percent_tags` int(11) NOT NULL DEFAULT 80,
  `min_percent_contents` int(11) NOT NULL DEFAULT 100,
  `min_percent_image_urls` int(11) NOT NULL DEFAULT 100,
  `min_percent_opinion` int(11) NOT NULL DEFAULT 0,
  `min_percent_paywall` int(11) NOT NULL DEFAULT 0,
  `min_average_authors` int(11) NOT NULL DEFAULT 1,
  `max_author_count` int(11) NOT NULL DEFAULT 5,
  `max_author_name_length` int(11) NOT NULL DEFAULT 40,
  `max_author_name_wordcount` int(11) NOT NULL DEFAULT 5,
  `min_average_tags` int(11) NOT NULL DEFAULT 1,
  `max_tag_count` int(11) NOT NULL DEFAULT 20,
  `max_tag_name_length` int(11) NOT NULL DEFAULT 50,
  `max_tag_name_wordcount` int(11) NOT NULL DEFAULT 5,
  `min_average_categories` int(11) NOT NULL DEFAULT 1,
  `max_category_count` int(11) NOT NULL DEFAULT 5,
  `max_category_name_length` int(11) NOT NULL DEFAULT 30,
  `max_category_name_wordcount` int(11) NOT NULL DEFAULT 3,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `id` (`id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statistics` (
  `source` int(11) NOT NULL,
  `hourly_position_count` int(11) DEFAULT NULL,
  `hourly_article_count` int(11) DEFAULT NULL,
  `hourly_crawl_result_count` int(11) DEFAULT NULL,
  `article_count` int(11) DEFAULT NULL,
  `modification_count` int(11) DEFAULT NULL,
  `highest_article_modification_count` int(11) DEFAULT NULL,
  `highest_url_modification_count` int(11) DEFAULT NULL,
  `highest_title_modification_count` int(11) DEFAULT NULL,
  `highest_description_modification_count` int(11) DEFAULT NULL,
  `highest_content_modification_count` int(11) DEFAULT NULL,
  `average_article_modifications` float DEFAULT NULL,
  `average_url_modifications` float DEFAULT NULL,
  `average_title_modifications` float DEFAULT NULL,
  `average_description_modifications` float DEFAULT NULL,
  `average_content_modifications` float DEFAULT NULL,
  `percent_modified` int(11) DEFAULT NULL,
  `percent_authors` int(11) DEFAULT NULL,
  `percent_categories` int(11) DEFAULT NULL,
  `percent_tags` int(11) DEFAULT NULL,
  `percent_contents` int(11) DEFAULT NULL,
  `percent_image_urls` int(11) DEFAULT NULL,
  `percent_opinion` int(11) DEFAULT NULL,
  `percent_paywall` int(11) DEFAULT NULL,
  `average_authors` float DEFAULT NULL,
  `highest_author_count` int(11) DEFAULT NULL,
  `highest_author_name_length` int(11) DEFAULT NULL,
  `highest_author_name_wordcount` int(11) DEFAULT NULL,
  `average_tags` float DEFAULT NULL,
  `highest_tag_count` int(11) DEFAULT NULL,
  `highest_tag_name_length` int(11) DEFAULT NULL,
  `highest_tag_name_wordcount` int(11) DEFAULT NULL,
  `average_categories` float DEFAULT NULL,
  `highest_category_count` int(11) DEFAULT NULL,
  `highest_category_name_length` int(11) DEFAULT NULL,
  `highest_category_name_wordcount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tag_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_links` (
  `article_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  UNIQUE KEY `article_id` (`article_id`,`tag_id`) USING BTREE,
  KEY `tag_id` (`tag_id`),
  KEY `article_id_2` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  FULLTEXT KEY `name_2` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `threshold_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `threshold_links` (
  `item_name` varchar(50) NOT NULL,
  `threshold_min` varchar(50) DEFAULT NULL,
  `threshold_max` varchar(50) DEFAULT NULL,
  `proc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`item_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `titles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `titles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `text` varchar(200) /*!100301 COMPRESSED*/ NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `urls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `urls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `url` text NOT NULL,
  `sha256` binary(64) GENERATED ALWAYS AS (sha2(`url`,256)) VIRTUAL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sha256` (`sha256`),
  UNIQUE KEY `id` (`id`,`article_id`),
  KEY `article_id` (`article_id`),
  FULLTEXT KEY `url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `visible`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visible` (
  `source` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `hours` int(11) NOT NULL,
  `average_position` int(11) NOT NULL,
  `updated` tinyint(4) NOT NULL DEFAULT 0,
  UNIQUE KEY `top10_unique` (`source`,`rank`),
  KEY `source` (`source`),
  KEY `article_id` (`article_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `refresh_position_stats` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 */ /*!50106 EVENT `refresh_position_stats` ON SCHEDULE EVERY 60 MINUTE ON COMPLETION NOT PRESERVE ENABLE DO CALL refresh_position_stats */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
/*!50106 DROP EVENT IF EXISTS `refresh_statistics` */;;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 */ /*!50106 EVENT `refresh_statistics` ON SCHEDULE EVERY 10 MINUTE ON COMPLETION NOT PRESERVE ENABLE DO CALL refresh_statistics */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
/*!50106 DROP EVENT IF EXISTS `refresh_visible` */;;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 */ /*!50106 EVENT `refresh_visible` ON SCHEDULE EVERY 30 MINUTE ON COMPLETION NOT PRESERVE ENABLE DO CALL refresh_visible */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `articles_without_authors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `articles_without_authors`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT
        latest_articles.url
    FROM
        latest_articles
    LEFT JOIN 
    	author_links 
        ON latest_articles.id = author_links.article_id
    WHERE
        author_links.author_id IS NULL
    GROUP BY
        latest_articles.id
	LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `articles_without_categories` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `articles_without_categories`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT
        latest_articles.url
    FROM
        latest_articles
    LEFT JOIN 
		category_links 
		ON latest_articles.id = category_links.article_id
    WHERE
        category_links.category_id IS NULL
    GROUP BY
        latest_articles.id
	LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `articles_without_contents` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `articles_without_contents`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT
        latest_articles.url
    FROM
        latest_articles
    WHERE
        (latest_articles.content IS NULL or latest_articles.content LIKE "") 
		AND 
		latest_articles.paywall = 0
    GROUP BY
        latest_articles.id
	LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `articles_without_tags` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `articles_without_tags`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT
        latest_articles.url
    FROM
        latest_articles
    LEFT JOIN 
		tag_links 
		ON latest_articles.id = tag_links.article_id
    WHERE
        tag_links.tag_id IS NULL
    GROUP BY
        latest_articles.id
	LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `call_procedure_from_item` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `call_procedure_from_item`(IN `item_name` VARCHAR(50) CHARSET utf8mb4, IN `source` INT)
BEGIN
SET @proc_name = (SELECT threshold_links.proc FROM threshold_links WHERE threshold_links.item_name = item_name);
PREPARE QRY FROM CONCAT("CALL ",@proc_name,"(",source,")");
EXECUTE QRY;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_latest_articles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `create_latest_articles`(IN `source` INT)
BEGIN
    DROP TEMPORARY TABLE IF EXISTS latest_articles;
    CREATE TEMPORARY TABLE 
        latest_articles 
		(INDEX article_id (id))
    SELECT
        articles.*
    FROM
        articles
    WHERE
        articles.source = source 
        AND 
        articles.date_downloaded >= (NOW() - INTERVAL 24 HOUR);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_article_modifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_article_modifications`(IN `source` INT)
BEGIN
    DROP TEMPORARY TABLE IF EXISTS latest_positions;
    CREATE TEMPORARY TABLE
        latest_positions
        (UNIQUE article_id (article_id))
    SELECT
        (COUNT(DISTINCT position_cache.url) - 1) AS url_modifications,
        urls.article_id
    FROM
        position_cache
    JOIN
        urls
        ON urls.id = position_cache.url
    GROUP BY
        urls.article_id;
    
    DROP TEMPORARY TABLE IF EXISTS latest_articles;
    CREATE TEMPORARY TABLE
        latest_articles
        (UNIQUE article_id (id), INDEX source (source))
    SELECT
        *
    FROM
        latest_positions
    JOIN
        articles
        ON latest_positions.article_id = articles.id
    WHERE
        articles.source = source;

	SELECT
		latest_articles.url_modifications
        + COUNT(DISTINCT titles.id) 
        + COUNT(DISTINCT descriptions.id) 
        + COUNT(DISTINCT contents.id) as modifications,
        latest_articles.url,
        latest_articles.url_modifications as url_count,
		COUNT(DISTINCT titles.id) as title_count,
		COUNT(DISTINCT descriptions.id) as description_count,
		COUNT(DISTINCT contents.id) as content_count,
        LEFT(latest_articles.title, 100) AS title,
        LEFT(latest_articles.description, 100) AS description,
        LEFT(latest_articles.content, 100) AS content
    FROM
        latest_articles
    LEFT JOIN
        titles 
        ON titles.article_id = latest_articles.id
        AND titles.date >= (NOW() - INTERVAL 24 HOUR)
    LEFT JOIN
        descriptions 
        ON descriptions.article_id = latest_articles.id
        AND descriptions.date >= (NOW() - INTERVAL 24 HOUR)
    LEFT JOIN
        contents 
        ON contents.article_id = latest_articles.id
        AND contents.date >= (NOW() - INTERVAL 24 HOUR)
    GROUP BY
    	latest_articles.id
    ORDER BY
        modifications DESC
    LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_author_count` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_author_count`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    DROP TEMPORARY TABLE IF EXISTS highest_count;
    CREATE TEMPORARY TABLE
        highest_count
    SELECT
        latest_articles.url,
        COUNT(author_links.author_id) AS author_count,
        latest_articles.id
    FROM
        latest_articles
    JOIN 
        author_links ON latest_articles.id = author_links.article_id
    GROUP BY
        latest_articles.id
    HAVING
        COUNT(author_links.author_id) > 0
    ORDER BY
        author_count DESC
    LIMIT 1;


    SELECT
        highest_count.url,
        LEFT(authors.name, 100) AS author
    FROM
        highest_count
    JOIN   
        author_links ON highest_count.id = author_links.article_id
    JOIN
        authors ON author_links.author_id = authors.id
    ORDER BY
        authors.name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_author_count_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_author_count_list`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT
    	latest_articles.url,
        COUNT(DISTINCT author_links.author_id) AS author_count
    FROM
        latest_articles
    JOIN 
		author_links 
		ON latest_articles.id = author_links.article_id
    GROUP BY
        latest_articles.id
    HAVING
        COUNT(author_links.author_id) > 0
    ORDER BY
        author_count DESC
	LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_author_name_length` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_author_name_length`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT 
        authors.name AS author,
        LENGTH(authors.name) AS author_name_length,
        latest_articles.url
    FROM
        latest_articles
    JOIN 
        author_links 
        ON latest_articles.id = author_links.article_id
    JOIN 
        authors 
        ON author_links.author_id = authors.id
    GROUP BY
        authors.name
    ORDER BY
        author_name_length DESC
    LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_author_name_wordcount` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_author_name_wordcount`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT 
        authors.name AS author,
        (LENGTH(authors.name) - LENGTH(REPLACE(authors.name, ' ', '')) + 1) AS author_word_count,
        latest_articles.url
    FROM
        latest_articles
    JOIN 
        author_links 
        ON latest_articles.id = author_links.article_id
    JOIN 
        authors 
        ON author_links.author_id = authors.id
    GROUP BY
        authors.name
    ORDER BY
        author_word_count DESC
    LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_category_count` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_category_count`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    DROP TEMPORARY TABLE IF EXISTS highest_count;
    CREATE TEMPORARY TABLE
        highest_count
    SELECT
        latest_articles.url,
        COUNT(category_links.category_id) AS category_count,
        latest_articles.id
    FROM
        latest_articles
    JOIN 
        category_links ON latest_articles.id = category_links.article_id
    GROUP BY
        latest_articles.id
    HAVING
        COUNT(category_links.category_id) > 0
    ORDER BY
        category_count DESC
    LIMIT 1;


    SELECT
        highest_count.url,
        LEFT(categories.name, 100) AS category
    FROM
        highest_count
    JOIN   
        category_links ON highest_count.id = category_links.article_id
    JOIN
        categories ON category_links.category_id = categories.id
    ORDER BY
        categories.name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_category_count_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_category_count_list`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT
    	latest_articles.url,
        COUNT(DISTINCT category_links.category_id) AS category_count
    FROM
        latest_articles
    JOIN 
		category_links 
		ON latest_articles.id = category_links.article_id
    GROUP BY
        latest_articles.id
    HAVING
        COUNT(category_links.category_id) > 0
    ORDER BY
        category_count DESC
	LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_category_name_length` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_category_name_length`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT 
        categories.name AS category,
        LENGTH(categories.name) AS category_name_length,
        latest_articles.url
    FROM
        latest_articles
    JOIN 
        category_links 
        ON latest_articles.id = category_links.article_id
    JOIN 
        categories 
        ON category_links.category_id = categories.id
    GROUP BY
        categories.name
    ORDER BY
        category_name_length DESC
    LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_category_name_wordcount` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_category_name_wordcount`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT 
        categories.name AS category,
        (LENGTH(categories.name) - LENGTH(REPLACE(categories.name, ' ', '')) + 1) AS category_word_count,
        latest_articles.url
    FROM
        latest_articles
    JOIN 
        category_links 
        ON latest_articles.id = category_links.article_id
    JOIN 
        categories 
        ON category_links.category_id = categories.id
    GROUP BY
        categories.name
    ORDER BY
        category_word_count DESC
    LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_content_modifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_content_modifications`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT
        (COUNT(DISTINCT contents.id)) AS modifications,
        latest_articles.url,
        LEFT(latest_articles.content, 100) as content
    FROM
        latest_articles
    JOIN 
		contents 
		ON latest_articles.id = contents.article_id
		AND contents.date >= (NOW() - INTERVAL 24 HOUR)
    GROUP BY
        latest_articles.id
    HAVING
    	(COUNT(DISTINCT contents.id)) > 0
    ORDER BY
        modifications DESC
	LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_description_modifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_description_modifications`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT
        (COUNT(DISTINCT descriptions.id)) AS modifications,
        latest_articles.url,
        LEFT(latest_articles.description, 100) as description
    FROM
        latest_articles
    JOIN 
		descriptions 
		ON latest_articles.id = descriptions.article_id
		AND descriptions.date >= (NOW() - INTERVAL 24 HOUR)
    GROUP BY
        latest_articles.id
    HAVING
    	(COUNT(DISTINCT descriptions.id)) > 0
    ORDER BY
        modifications DESC
	LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_tag_count` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_tag_count`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    DROP TEMPORARY TABLE IF EXISTS highest_count;
    CREATE TEMPORARY TABLE
        highest_count
    SELECT
        latest_articles.url,
        COUNT(tag_links.tag_id) AS tag_count,
        latest_articles.id
    FROM
        latest_articles
    JOIN 
        tag_links ON latest_articles.id = tag_links.article_id
    GROUP BY
        latest_articles.id
    HAVING
        COUNT(tag_links.tag_id) > 0
    ORDER BY
        tag_count DESC
    LIMIT 1;


    SELECT
        highest_count.url,
        LEFT(tags.name, 100) AS tag
    FROM
        highest_count
    JOIN   
        tag_links ON highest_count.id = tag_links.article_id
    JOIN
        tags ON tag_links.tag_id = tags.id
    ORDER BY
        tags.name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_tag_count_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_tag_count_list`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT
    	latest_articles.url,
        COUNT(DISTINCT tag_links.tag_id) AS tag_count
    FROM
        latest_articles
    JOIN 
		tag_links 
		ON latest_articles.id = tag_links.article_id
    GROUP BY
        latest_articles.id
    HAVING
        COUNT(tag_links.tag_id) > 0
    ORDER BY
        tag_count DESC
	LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_tag_name_length` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_tag_name_length`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT 
        tags.name AS tag,
        LENGTH(tags.name) AS tag_name_length,
        latest_articles.url
    FROM
        latest_articles
    JOIN 
        tag_links 
        ON latest_articles.id = tag_links.article_id
    JOIN 
        tags 
        ON tag_links.tag_id = tags.id
    GROUP BY
        tags.name
    ORDER BY
        tag_name_length DESC
    LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_tag_name_wordcount` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_tag_name_wordcount`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT 
        tags.name AS tag,
        (LENGTH(tags.name) - LENGTH(REPLACE(tags.name, ' ', '')) + 1) AS tag_word_count,
        latest_articles.url
    FROM
        latest_articles
    JOIN 
        tag_links 
        ON latest_articles.id = tag_links.article_id
    JOIN 
        tags 
        ON tag_links.tag_id = tags.id
    GROUP BY
        tags.name
    ORDER BY
        tag_word_count DESC
    LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_title_modifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_title_modifications`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT
        (COUNT(DISTINCT titles.id)) AS modifications,
        latest_articles.url,
        LEFT(latest_articles.title, 100) as title
    FROM
        latest_articles
    JOIN 
		titles 
		ON latest_articles.id = titles.article_id
		AND titles.date >= (NOW() - INTERVAL 24 HOUR)
    GROUP BY
        latest_articles.id
    HAVING
    	(COUNT(DISTINCT titles.id)) > 0
    ORDER BY
        modifications DESC
	LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `highest_url_modifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `highest_url_modifications`(IN `source` INT)
BEGIN
    CALL create_latest_articles(source);

    SELECT
    	urls.url,
        COUNT(DISTINCT position_cache.url) AS url_count
    FROM
        latest_articles
    JOIN 
		urls 
		ON latest_articles.id = urls.article_id
	JOIN
		position_cache
		ON position_cache.url = urls.id
    GROUP BY
        latest_articles.id
    HAVING
        COUNT(position_cache.url) > 0
    ORDER BY
        url_count DESC
	LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `refresh_position_stats` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `refresh_position_stats`()
BEGIN

DROP TEMPORARY TABLE IF EXISTS new_positions;
CREATE TEMPORARY TABLE
	new_positions
SELECT
	position_cache.url,
	position_cache.date AS date_id,
	position_cache.position,
	position_cache.date
FROM
	position_cache
WHERE
	position_cache.analyzed = 0
	AND
	HOUR(position_cache.date) < HOUR(NOW());

UPDATE
    articles
JOIN 
(
	SELECT
	    positions_id.article_id,
	    COUNT(DISTINCT positions_id.date) AS hours,
	    ROUND(SUM(positions_id.position) / COUNT(DISTINCT positions_id.date)) AS average_position,
	    ROUND(SUM(1 / POW(positions_id.position, 2)) * 1000) AS weight,
		MIN(positions_id.position) AS highest_position,
		MAX(positions_id.position) AS lowest_position,
		MIN(positions_id.date) as first_seen,
		MAX(positions_id.date) as last_seen
	FROM
	    (
			SELECT
				articles.id as article_id,
				new_positions.date,
				ROUND(SUM(new_positions.position) / COUNT(new_positions.date_id)) AS position
			FROM
				new_positions
            JOIN
                urls
                ON new_positions.url = urls.id
			JOIN
				articles 
                ON articles.id = urls.article_id
			GROUP BY
				articles.id, 
				DATE(new_positions.date), 
				HOUR(new_positions.date)
	    ) as positions_id
	GROUP BY
	    positions_id.article_id
) AS stats ON stats.article_id = articles.id
SET
	articles.hours = IF(articles.hours IS NULL, stats.hours, articles.hours + stats.hours),
	articles.average_position = IF(articles.average_position IS NULL, stats.average_position, 
		ROUND(
			((articles.average_position * articles.hours) + (stats.average_position * stats.hours)) / (articles.hours + stats.hours)
		)
	),
	articles.weight = IF(articles.weight IS NULL, stats.weight, articles.weight + stats.weight),
	articles.highest_position = IF(stats.highest_position < articles.highest_position OR articles.highest_position IS NULL, stats.highest_position, articles.highest_position),
	articles.lowest_position = IF(stats.lowest_position > articles.lowest_position OR articles.lowest_position IS NULL, stats.lowest_position, articles.lowest_position),
	articles.first_seen = IF(stats.first_seen < articles.first_seen OR articles.first_seen IS NULL, stats.first_seen, articles.first_seen),
	articles.last_seen = IF(stats.last_seen > articles.last_seen OR articles.last_seen IS NULL, stats.last_seen, articles.last_seen);

UPDATE
	position_cache
JOIN
	new_positions
	ON position_cache.url = new_positions.url
	AND position_cache.date = new_positions.date_id
SET
	position_cache.analyzed = 1
WHERE
	position_cache.analyzed = 0;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `refresh_statistics` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `refresh_statistics`()
BEGIN

TRUNCATE TABLE statistics;

DROP TEMPORARY TABLE IF EXISTS hourly_positions;
CREATE TEMPORARY TABLE
    hourly_positions
    (UNIQUE article_id (article_id))
SELECT
    COUNT(position_cache.position) AS position_count,
    urls.article_id
FROM
    position_cache
JOIN
    urls
    ON urls.id = position_cache.url
WHERE
	position_cache.date >= (NOW() - INTERVAL 3610 SECOND)
GROUP BY
    urls.article_id;


DROP TEMPORARY TABLE IF EXISTS hourly_articles_all;
CREATE TEMPORARY TABLE
    hourly_articles
    (INDEX source (source))
SELECT
    SUM(hourly_positions.position_count) as position_count,
    articles.source
FROM
    articles
JOIN
    hourly_positions
    ON hourly_positions.article_id = articles.id
GROUP BY 
    articles.source;


DROP TEMPORARY TABLE IF EXISTS latest_positions;
CREATE TEMPORARY TABLE
    latest_positions
    (UNIQUE article_id (article_id))
SELECT
    (COUNT(DISTINCT position_cache.url) - 1) AS url_modifications,
    COUNT(position_cache.position) AS position_count,
    urls.article_id
FROM
    position_cache
JOIN
    urls
    ON urls.id = position_cache.url
GROUP BY
    urls.article_id;


DROP TEMPORARY TABLE IF EXISTS latest_articles_all;
CREATE TEMPORARY TABLE
    latest_articles_all
    (UNIQUE article_id (id), INDEX source (source))
SELECT
    *
FROM
    articles
JOIN
    latest_positions
    ON latest_positions.article_id = articles.id;


DROP TEMPORARY TABLE IF EXISTS latest_articles;

CREATE TEMPORARY TABLE
    latest_articles
    (
        UNIQUE article_id (article_id), 
        INDEX source (source), 
        INDEX date_downloaded (date_downloaded)
    )
SELECT
    latest_articles_all.*,

    latest_articles_all.url_modifications
        + COUNT(DISTINCT titles.id) 
        + COUNT(DISTINCT descriptions.id) 
        + COUNT(DISTINCT contents.id) as modification_count,
    COUNT(DISTINCT titles.id) as title_modifications,
    COUNT(DISTINCT descriptions.id) as description_modifications,
    COUNT(DISTINCT contents.id) as content_modifications
FROM
    latest_articles_all
LEFT JOIN
    titles 
    ON titles.article_id = latest_articles_all.id
    AND titles.date >= (NOW() - INTERVAL 24 HOUR)
LEFT JOIN
    descriptions 
    ON descriptions.article_id = latest_articles_all.id
    AND descriptions.date >= (NOW() - INTERVAL 24 HOUR)
LEFT JOIN
    contents 
    ON contents.article_id = latest_articles_all.id
    AND contents.date >= (NOW() - INTERVAL 24 HOUR)
GROUP BY
    latest_articles_all.article_id;


INSERT INTO statistics (
    source,
    hourly_position_count,
    hourly_article_count,
    hourly_crawl_result_count,
    article_count,
    modification_count,
    highest_article_modification_count,
    highest_url_modification_count,
    highest_title_modification_count,
    highest_description_modification_count,
    highest_content_modification_count,
    average_article_modifications,
    average_url_modifications,
    average_title_modifications,
    average_description_modifications,
    average_content_modifications,
    percent_modified,
    percent_authors,
    percent_categories,
    percent_tags,
    percent_contents,
    percent_image_urls,
    percent_opinion,
    percent_paywall,
    average_authors,
    highest_author_count,
    highest_author_name_length,
    highest_author_name_wordcount,
    average_tags,
    highest_tag_count,
    highest_tag_name_length,
    highest_tag_name_wordcount,
    average_categories,
    highest_category_count,
    highest_category_name_length,
    highest_category_name_wordcount
)
SELECT
    sources.id as source,
    
    hourly_articles.position_count as hourly_position_count,
    last_articles.article_count as hourly_article_count,
    last_crawl_results.crawl_result_count as hourly_crawl_result_count,

    COUNT(
        DISTINCT latest_articles.article_id
    ) AS article_count,
    SUM(modifications.modification_count) as modification_count,

    MAX(modifications.modification_count) as highest_article_modification_count,
    MAX(latest_articles.url_modifications) as highest_url_modification_count,
    MAX(latest_articles.title_modifications) as highest_title_modification_count,
    MAX(latest_articles.description_modifications) as highest_description_modification_count,
    MAX(latest_articles.content_modifications) as highest_content_modification_count,

    ROUND(
        SUM(modifications.modification_count) / COUNT(DISTINCT latest_articles.article_id),
        2
    ) as average_article_modifications,

    ROUND(
        SUM(latest_articles.url_modifications) / COUNT(DISTINCT latest_articles.article_id),
        2
    ) as average_url_modifications,

    ROUND(
        SUM(latest_articles.title_modifications) / COUNT(DISTINCT latest_articles.article_id),
        2
    ) as average_title_modifications,
    
    ROUND(
        SUM(latest_articles.description_modifications) / COUNT(DISTINCT latest_articles.article_id),
        2
    ) as average_description_modifications,

    ROUND(
        SUM(latest_articles.content_modifications) / COUNT(DISTINCT latest_articles.article_id),
        2
    ) as average_content_modifications,

    
    ROUND(
        COUNT(DISTINCT modifications.article_id) / NULLIF(COUNT(DISTINCT latest_articles.article_id), 0) * 100,
        2
    ) AS percent_modified,

    
    ROUND(
        COUNT(DISTINCT author_links2.article_id) / NULLIF(COUNT(DISTINCT latest_articles.article_id), 0) * 100,
        2
    ) AS percent_authors,

    
    ROUND(
        COUNT(DISTINCT category_links2.article_id) / NULLIF(COUNT(DISTINCT latest_articles.article_id), 0) * 100,
        2
    ) AS percent_categories,
    
    
    ROUND(
        COUNT(DISTINCT tag_links2.article_id) / NULLIF(COUNT(DISTINCT latest_articles.article_id), 0) * 100,
        2
    ) AS percent_tags,


    ROUND(
        COUNT(content_articles.article_id) / NULLIF(COUNT(DISTINCT latest_articles.article_id), 0) * 100,
        2
    ) AS percent_contents,

    ROUND(
        COUNT(image_url_articles.article_id) / NULLIF(COUNT(DISTINCT latest_articles.article_id), 0) * 100,
        2
    ) AS percent_image_urls,

    ROUND(
        COUNT(opinion_articles.article_id) / NULLIF(COUNT(DISTINCT latest_articles.article_id), 0) * 100,
        2
    ) AS percent_opinion,

    ROUND(
        COUNT(paywall_articles.article_id) / NULLIF(COUNT(DISTINCT latest_articles.article_id), 0) * 100,
        2
    ) AS percent_paywall,


    ROUND(
        SUM(author_links2.author_count) / SUM(author_links2.author_article_count),
        2
    ) AS average_authors,
    MAX(author_links2.author_count) as highest_author_count,
    MAX(author_links2.author_name_length) as highest_author_name_length,
    MAX(author_links2.author_name_wordcount) as highest_author_name_wordcount,

    
    ROUND(
        SUM(tag_links2.tag_count) / SUM(tag_links2.tag_article_count),
        2
    ) AS average_tags,
    MAX(tag_links2.tag_count) as highest_tag_count,
    MAX(tag_links2.tag_name_length) as highest_tag_name_length,
    MAX(tag_links2.tag_name_wordcount) as highest_tag_name_wordcount,

    
    ROUND(
        SUM(category_links2.category_count) / SUM(category_links2.category_article_count),
        2
    ) AS average_categories,
    MAX(category_links2.category_count) as highest_category_count,
    MAX(category_links2.category_name_length) as highest_category_name_length,
    MAX(category_links2.category_name_wordcount) as highest_category_name_wordcount

FROM
    sources

LEFT JOIN latest_articles ON latest_articles.source = sources.id


LEFT JOIN (
    SELECT latest_articles.article_id, latest_articles.modification_count
    FROM latest_articles
    WHERE latest_articles.modification_count > 0
)   as modifications on latest_articles.article_id = modifications.article_id


LEFT JOIN (
    SELECT latest_articles.article_id
    FROM latest_articles
    WHERE (
        latest_articles.content IS NOT NULL
        AND latest_articles.content != ""
        AND latest_articles.paywall = 0
    ) OR latest_articles.paywall = 1
) as content_articles on latest_articles.article_id = content_articles.article_id

LEFT JOIN (
    SELECT latest_articles.article_id
    FROM latest_articles
    WHERE latest_articles.image_url IS NOT NULL
    AND latest_articles.image_url != ""
) as image_url_articles on latest_articles.article_id = image_url_articles.article_id

LEFT JOIN (
    SELECT latest_articles.article_id
    FROM latest_articles
    WHERE latest_articles.opinion = 1
) as opinion_articles on latest_articles.article_id = opinion_articles.article_id


LEFT JOIN (
    SELECT latest_articles.article_id
    FROM latest_articles
    WHERE latest_articles.paywall = 1
) as paywall_articles on latest_articles.article_id = paywall_articles.article_id



LEFT JOIN hourly_articles 
    ON sources.id = hourly_articles.source

LEFT JOIN (
 	SELECT COUNT(*) as article_count, latest_articles.source
    FROM latest_articles
    WHERE latest_articles.date_downloaded >= (NOW() - INTERVAL 3610 SECOND)
    GROUP BY latest_articles.source
) as last_articles ON sources.id = last_articles.source

LEFT JOIN (
 	SELECT COUNT(*) as crawl_result_count, crawl_results.source
    FROM crawl_results
    WHERE crawl_results.date >= (NOW() - INTERVAL 3610 SECOND)
    GROUP BY crawl_results.source
) as last_crawl_results ON sources.id = last_crawl_results.source



LEFT JOIN (
    SELECT
    	author_links.article_id,
    	COUNT(DISTINCT author_links.article_id) as author_article_count,
    	COUNT(author_links.author_id) as author_count,
    	MAX(LENGTH(authors.name)) as author_name_length,
    	MAX(LENGTH(authors.name) - LENGTH(REPLACE(authors.name, ' ', '')) + 1) as author_name_wordcount
    FROM author_links
    JOIN latest_articles ON author_links.article_id = latest_articles.article_id
    JOIN authors on author_links.author_id = authors.id
    GROUP BY latest_articles.article_id
) as author_links2 ON author_links2.article_id = latest_articles.article_id

LEFT JOIN (
    SELECT
    	category_links.article_id,
    	COUNT(DISTINCT category_links.article_id) as category_article_count,
    	COUNT(category_links.category_id) as category_count,
        MAX(LENGTH(categories.name)) as category_name_length,
    	MAX(LENGTH(categories.name) - LENGTH(REPLACE(categories.name, ' ', '')) + 1) as category_name_wordcount
    FROM category_links
    JOIN latest_articles ON category_links.article_id = latest_articles.article_id
    JOIN categories on category_links.category_id = categories.id
    GROUP BY latest_articles.article_id
) as category_links2 ON category_links2.article_id = latest_articles.article_id

LEFT JOIN (
    SELECT
    	tag_links.article_id,
    	COUNT(DISTINCT tag_links.article_id) as tag_article_count,
    	COUNT(tag_links.tag_id) as tag_count,
        MAX(LENGTH(tags.name)) as tag_name_length,
    	MAX(LENGTH(tags.name) - LENGTH(REPLACE(tags.name, ' ', '')) + 1) as tag_name_wordcount
    FROM tag_links
    JOIN latest_articles ON tag_links.article_id = latest_articles.article_id
    JOIN tags on tag_links.tag_id = tags.id
    GROUP BY latest_articles.article_id
) as tag_links2 ON tag_links2.article_id = latest_articles.article_id


GROUP BY sources.id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `refresh_visible` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE  PROCEDURE `refresh_visible`()
BEGIN

DELETE
	position_cache
FROM
	position_cache
WHERE
	position_cache.date < (NOW() - INTERVAL 1 DAY);


DROP TEMPORARY TABLE IF EXISTS rating;
CREATE TEMPORARY TABLE
	rating
SELECT
	COUNT(article_pos.date) as hours,
	SUM(1 / POW(article_pos.position, 2)) as weight,
	ROUND(SUM(article_pos.position) / COUNT(article_pos.article_id)) as average,
	article_pos.source,
    MAX(article_pos.article_id) as article_id
FROM
(
	SELECT
		articles.id as article_id,
		articles.source,
		position_cache.date,
		ROUND(SUM(position_cache.position) / COUNT(position_cache.date)) AS position
	FROM
		position_cache
	JOIN
		urls
		ON urls.id = position_cache.url
	JOIN
		articles ON urls.article_id = articles.id
	GROUP BY
		articles.id,
		DATE(position_cache.date), 
		HOUR(position_cache.date)
) AS article_pos
GROUP BY
	article_pos.article_id;

 
UPDATE visible SET updated = 0;

INSERT INTO
    visible (source, rank, weight, article_id, hours, average_position, updated)
SELECT 
    grouped_rating.source,
    grouped_rating.rank,
    ROUND(grouped_rating.weight * 1000) as weight,
    grouped_rating.article_id,
    grouped_rating.hours,
    grouped_rating.average,
	1
FROM 
    (
        SELECT 
            rating.*,
            ROW_NUMBER() OVER (PARTITION BY rating.source ORDER BY rating.weight DESC) AS rank
        FROM rating
    ) as grouped_rating
WHERE 
    grouped_rating.rank <= 100
ON DUPLICATE KEY UPDATE 
    weight = ROUND(grouped_rating.weight * 1000),
    article_id = grouped_rating.article_id,
    hours = grouped_rating.hours,
    average_position = grouped_rating.average,
	updated = 1;

DELETE FROM visible WHERE updated = 0;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `sources` WRITE;
/*!40000 ALTER TABLE `sources` DISABLE KEYS */;
INSERT INTO `sources` VALUES
(1,'Spiegel',207331150,10,0,1,10,0,15,2,90,100,80,80,100,1,10,1,25,40,5,1,30,55,10,1,5,30,3),
(2,'rnd',89999942,10,0,1,10,0,15,2,80,100,80,80,100,1,1,1,10,50,6,1,20,50,5,1,5,30,3),
(3,'taz',14030009,10,0,1,10,0,15,2,100,95,80,100,100,1,0,1,7,40,5,1,20,55,10,1,5,30,3),
(4,'Tagesschau',169430000,10,0,1,10,0,15,2,10,95,80,100,98,0,0,1,10,40,5,1,20,50,5,1,5,30,5),
(5,'FAZ',74153855,10,0,1,10,0,50,2,90,99,80,85,100,1,1,1,8,40,5,1,25,60,10,1,5,40,5),
(6,'Welt',129834610,10,0,1,10,0,40,2,80,95,90,80,100,1,1,0,15,40,5,0,120,60,10,0,5,30,3),
(7,'Focus',214753788,10,0,1,10,0,15,2,70,80,1,95,100,0,0,1,25,40,5,1,200,60,10,1,6,32,5),
(8,'Bild',488732100,10,0,1,10,0,100,2,50,99,80,70,100,100,1,1,25,35,4,1,50,60,10,1,5,40,10),
(9,'Handelsblatt',31155375,10,0,1,10,0,35,2,100,100,1,0,100,0,10,1,60,40,5,1,2000,60,10,1,5,30,3),
(10,'Zeit',78468674,10,0,1,10,0,15,2,100,100,80,90,100,0,1,1,120,40,5,1,100,60,15,1,5,30,3),
(11,'Heise',34392426,10,0,1,10,0,15,2,95,100,80,100,100,0,1,1,7,40,5,1,40,52,10,1,5,30,5),
(12,'Telepolis',1300000,10,0,1,1,0,15,2,90,100,80,100,100,100,0,1,5,40,5,1,20,50,5,1,5,30,3),
(13,'Stern',68972492,10,0,1,10,0,30,2,80,98,80,90,99,0,1,1,15,40,5,1,300,60,10,1,6,60,10),
(14,'n-tv',252297101,10,0,1,10,0,220,2,70,100,80,95,95,0,0,1,50,40,5,1,500,55,6,1,5,30,3),
(15,'SZ',65003397,10,0,1,10,0,15,2,80,100,80,30,95,1,1,1,35,40,5,1,35,60,10,1,5,30,3),
(16,'Junge Freiheit',2900000,10,0,1,1,0,15,2,70,100,80,100,100,100,1,1,5,40,5,1,20,50,7,1,5,30,3),
(17,'Tichys Einblick',4345027,10,0,1,10,0,15,2,50,95,0,100,100,100,0,1,5,40,5,0,20,50,5,1,5,35,5),
(18,'Neues Deutschland',866000,10,0,1,10,0,15,2,80,100,80,100,100,0,0,1,5,40,5,1,20,50,5,1,5,30,3),
(19,'Tagesspiegel',41960200,10,0,1,10,0,25,2,100,90,70,100,95,0,0,1,20,40,5,1,30,50,5,1,5,40,6);
/*!40000 ALTER TABLE `sources` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `threshold_links` WRITE;
/*!40000 ALTER TABLE `threshold_links` DISABLE KEYS */;
INSERT INTO `threshold_links` VALUES
('article_count','min_article_count',NULL,NULL),
('average_article_modifications',NULL,'max_average_modification_count','highest_article_modifications'),
('average_authors','min_average_authors',NULL,'highest_author_count_list'),
('average_categories','min_average_categories',NULL,'highest_category_count_list'),
('average_content_modifications',NULL,'max_average_modification_count','highest_content_modifications'),
('average_description_modifications',NULL,'max_average_modification_count','highest_description_modifications'),
('average_tags','min_average_tags',NULL,'highest_tag_count_list'),
('average_title_modifications',NULL,'max_average_modification_count','highest_title_modifications'),
('average_url_modifications',NULL,'max_average_modification_count','highest_url_modifications'),
('highest_article_modification_count',NULL,'max_property_modification_count','highest_article_modifications'),
('highest_author_count',NULL,'max_author_count','highest_author_count'),
('highest_author_name_length',NULL,'max_author_name_length','highest_author_name_length'),
('highest_author_name_wordcount',NULL,'max_author_name_wordcount','highest_author_name_wordcount'),
('highest_category_count',NULL,'max_category_count','highest_category_count'),
('highest_category_name_length',NULL,'max_category_name_length','highest_category_name_length'),
('highest_category_name_wordcount',NULL,'max_category_name_wordcount','highest_category_name_wordcount'),
('highest_content_modification_count',NULL,'max_property_modification_count','highest_description_modifications'),
('highest_description_modification_count',NULL,'max_property_modification_count','highest_description_modifications'),
('highest_tag_count',NULL,'max_tag_count','highest_tag_count'),
('highest_tag_name_length',NULL,'max_tag_name_length','highest_tag_name_length'),
('highest_tag_name_wordcount',NULL,'max_tag_name_wordcount','highest_tag_name_wordcount'),
('highest_title_modification_count',NULL,'max_property_modification_count','highest_title_modifications'),
('highest_url_modification_count',NULL,'max_property_modification_count','highest_url_modifications'),
('hourly_article_count','min_hourly_article_count',NULL,NULL),
('hourly_crawl_result_count','min_hourly_crawl_result_count',NULL,NULL),
('hourly_position_count','min_hourly_position_count',NULL,NULL),
('modification_count','min_modification_count',NULL,NULL),
('percent_authors','min_percent_authors',NULL,'articles_without_authors'),
('percent_categories','min_percent_categories',NULL,'articles_without_categories'),
('percent_contents','min_percent_contents',NULL,'articles_without_contents'),
('percent_image_urls','min_percent_image_urls',NULL,NULL),
('percent_modified','min_article_modification_count',NULL,NULL),
('percent_opinion','min_percent_opinion',NULL,NULL),
('percent_paywall','min_percent_paywall',NULL,NULL),
('percent_tags','min_percent_tags',NULL,'articles_without_tags');
/*!40000 ALTER TABLE `threshold_links` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

