# news-crawler
A crawler for news sites  
  
- Positions of articles on the landing page of news sites are regulary saved.  
- Links authors, tags, channels/categories to articles.   
- Saves full body text, all article modifications and whether article has a paywall or is an opinion article.
- Efficient disk space saving database structure
- Parallel worker threads

This project aims to create a dataset upon wich lot's of things about the mass media could hopefully be analyzed one day.  
- The influence of mass media on the audience - for example by comparing the most prominent articles of news sites and their topics with surveys
- What proportion of which topic is on which news site?
- Wich article was the primary source of information?
- Who set (started) a topic or set a buzzword and wrote first about it - and how did it spread? (lot's of useless discussions - from a science perspective - are dominating the headlines while very important topics get buried)
- Follow editor teams or journalists activities
- Collect facts and numbers for biases and topic weighting
- Lot's of interesting graphics and (interactive) visualization could be created to get an overview of the mass media situation based on evidence and facts rather than gut instincts 
- Possible future uses of the data for machine learning projects like [Felix Hamborg's project's](https://github.com/fhamborg/NewsMTSC)

### Why did you create this crawler instead of using one already out there?
Because other crawlers i tried haven't been reliable enough, mostly didn't get the body text and couldn't analyse the landing page for positioning.




# Requirements
python 3.7+  
some python libs, see [requirements.txt](requirements.txt)  
mariadb  

The space required is about 1 MB / site / day (with an 1 hour interval)  
The traffic is hard to tell, but at most 200 MB per site / day (with an 1 hour interval)  

Optional: [news-crawler-web](https://gitlab.com/abocado/news-crawler-web)
and [news-crawler-watchdog](https://gitlab.com/abocado/news-crawler-watchdog)


# Getting started
`git clone https://gitlab.com/news-crawler1/news-crawler.git`  
All commands require you to be in the projects root directory  
`cd news-crawler`  

Copy `config.example.ini` to `config.ini`.  
Insert your MySQL credentials and adjust settings.  


## Install python module requirements
`pip install -r requirements.txt --user`  
( if multiple python versions are installed you might need to specify version like `pip3.7` )


## Install database structure
`cd db`  
`./install.sh`  
`cd ..`  

## Docker
`docker-compose run news-crawler`

## Run
`./news-crawler`  
starts all site crawlers placed in `sites/`  
( if multiple python versions are installed you have to start the script like `python3.7 news-crawler.py` )  

### Debug / Arguments examples
`./news-crawler --site spiegel`  
runs only the crawler from `sites/spiegel.py`  
  
`./news-crawler --site spiegel --landing`  
parses landing page of site and prints all links and positions found  

`./news-crawler --site spiegel --article`  
prints all found information for every article  

`nohup ./news-crawler --quiet &> errors.log &`  
run in background and write errors to logfile

`./news-crawler --daemon &> errors.log &`  
daemonize and write errors to logfile

### Article grouping
There is a module that groups hot topics of the last 24h  
Activate it by setting `article_grouping = true` in config.ini (and restart)  
Following requirements must be met:  
`pip install spacy`  
`python -m spacy download de_core_news_sm` (or whatever set you like)  

You can provide the path to a stopwords text file in config.ini.  
A utf-8 encoded text file is expected with one stopword per line (lowercase)  
Example: https://github.com/stopwords-iso/stopwords-de/blob/master/stopwords-de.txt

You can also provide the path to a text file which contains compound split words  
This is especially relevant in german language which has many compound words.  
A utf-8 encoded text file is expected with each line containing one compound word 
followed by its components,
seperated by tabs (\t 0x09).  
Example line for the german word Subventionspolitik:  
Subventionspolitik	Subvention	Politik  
Example file: https://sfs.uni-tuebingen.de/GermaNet/documents/compounds/split_compounds_from_GermaNet18.0.txt




## news-crawler-watchdog
It is highly recommended to run [news-crawler-watchdog](https://gitlab.com/abocado/news-crawler-watchdog)
(requires [news-crawler-web](https://gitlab.com/abocado/news-crawler-web))  

This is a watchdog, that notifies you with an email
once configured thresholds are exceeded.  
The thresholds can be configured in the dashboard of news-crawler-web.  
It also sends an email on log entries with level ERROR or CRITICAL.  

This watchdog can also be run from other locations, to ensure notification emails 
are sent even if the server running news-crawler is offline.  


# Development
### Create requirements.txt
`pip install pipreqs`  
`pipreqs --mode gt .`

### Dump database structure if changed
`cd db`  
`./dump.sh`

